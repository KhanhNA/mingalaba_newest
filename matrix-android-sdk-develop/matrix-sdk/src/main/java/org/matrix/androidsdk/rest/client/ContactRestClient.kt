package org.matrix.androidsdk.rest.client

import org.matrix.androidsdk.HomeServerConnectionConfig
import org.matrix.androidsdk.RestClient
import org.matrix.androidsdk.core.JsonUtils
import org.matrix.androidsdk.core.callback.ApiCallback
import org.matrix.androidsdk.rest.api.ContactApi
import org.matrix.androidsdk.rest.callback.RestAdapterCallback
import org.matrix.androidsdk.rest.model.ListContactIdetify
import org.matrix.androidsdk.rest.model.terms.ListContactRespone

class ContactRestClient(hsConfig: HomeServerConnectionConfig) :
        RestClient<ContactApi>(hsConfig, ContactApi::class.java, URI_API_PREFIX_PATH_R0, JsonUtils.getGson(true)) {

    fun getContactIdentify(lstContactLocal:ArrayList<String>,callback: ApiCallback<ListContactRespone>) {
        val arrayContactTest = ListContactIdetify(lstContactLocal)
        mApi.getContactIdentify(arrayContactTest).enqueue(RestAdapterCallback("getContactIdentify", null, callback, null))
    }
}