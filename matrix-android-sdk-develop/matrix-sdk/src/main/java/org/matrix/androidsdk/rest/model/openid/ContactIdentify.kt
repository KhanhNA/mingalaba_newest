package org.matrix.androidsdk.rest.model.openid

import com.google.gson.annotations.SerializedName

data class ContactIdentify(
        /**
         * Required. An access token the consumer may use to verify the identity of the person who generated the token.
         * This is given to the federation API GET /openid/userinfo.
         */
        @JvmField
        @SerializedName("user_id")
        val user_id: String,

        /**
         * Required. The string Bearer.
         */
        @JvmField
        @SerializedName("medium")
        val medium: String,

        /**
         * Required. The homeserver domain the consumer should use when attempting to verify the user's identity.
         */
        @JvmField
        @SerializedName("address")
        val address: String,
        /**
         * Required. The homeserver domain the consumer should use when attempting to verify the user's identity.
         */
        @JvmField
        @SerializedName("display_name")
        val display_name: String,

        /**
         * Required. The homeserver domain the consumer should use when attempting to verify the user's identity.
         */
        @JvmField
        @SerializedName("avatar_url")
        val avatar_url: String

)
