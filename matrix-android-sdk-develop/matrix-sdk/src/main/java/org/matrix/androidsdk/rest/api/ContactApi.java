package org.matrix.androidsdk.rest.api;

import org.matrix.androidsdk.rest.model.ListContactIdetify;
import org.matrix.androidsdk.rest.model.terms.ListContactRespone;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface ContactApi {

    /**
     * Check that we can use the identity server. You'll get a 403 if this is not the case
     */

    @POST("user_directory/get_list_user_exist")
        Call<ListContactRespone> getContactIdentify(@Body ListContactIdetify list_phone_number);
}

