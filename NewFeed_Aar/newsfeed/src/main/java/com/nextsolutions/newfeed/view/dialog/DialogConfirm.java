package com.nextsolutions.newfeed.view.dialog;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;

import com.nextsolutions.newfeed.R;
import com.nextsolutions.newfeed.databinding.DialogConfirmNewfeedBinding;
import com.nextsolutions.newfeed.util.StringUtils;

public class DialogConfirm extends DialogFragment {
    private String title;
    private String actionTitle;
    private String msg;
    private View.OnClickListener onClickListener;
    private boolean isCancelable;
    private boolean isHideCancelButton;

    public DialogConfirm(@NonNull String title, String msg, boolean isCancelable, boolean isHideCancelButton, View.OnClickListener onClickListener) {
        this.msg = msg;
        this.title = title;
        this.onClickListener = onClickListener;
        this.isCancelable = isCancelable;
        this.isHideCancelButton = isHideCancelButton;
    }

    public DialogConfirm(@NonNull String title, String msg, View.OnClickListener onClickListener) {
        this.msg = msg;
        this.title = title;
        this.onClickListener = onClickListener;
        this.isCancelable = true;
    }
    public DialogConfirm(@NonNull String title, String msg, String actionTitle, View.OnClickListener onClickListener) {
        this.msg = msg;
        this.title = title;
        this.onClickListener = onClickListener;
        this.actionTitle = actionTitle;
        this.isCancelable = true;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        DialogConfirmNewfeedBinding binding = DataBindingUtil.inflate(inflater, R.layout.dialog_confirm_newfeed, container, false);
        if (getDialog() != null && getDialog().getWindow() != null) {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }
        setCancelable(isCancelable);
        binding.btnCancel.setVisibility(isCancelable ? View.VISIBLE : View.INVISIBLE);
        binding.btnDismiss.setOnClickListener(v -> this.dismiss());
        binding.txtTitle.setText(title);

        if(actionTitle != null){
            binding.btnConfirm.setText(actionTitle);
        }
        if (StringUtils.isNullOrEmpty(msg)) {
            binding.txtMsg.setVisibility(View.GONE);
        } else {
            binding.txtMsg.setText(msg);
        }
        if (isHideCancelButton) {
            binding.btnDismiss.setVisibility(View.GONE);
        }

        binding.btnCancel.setOnClickListener(v -> this.dismiss());
        binding.btnConfirm.setOnClickListener(onClickListener);
        return binding.getRoot();
    }

}