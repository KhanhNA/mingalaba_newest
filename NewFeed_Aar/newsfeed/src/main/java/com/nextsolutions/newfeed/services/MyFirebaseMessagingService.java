//package com.nextsolutions.newfeed.services;
//
//import android.annotation.SuppressLint;
//import android.app.Notification;
//import android.app.NotificationChannel;
//import android.app.NotificationManager;
//import android.app.PendingIntent;
//import android.content.Context;
//import android.content.Intent;
//import android.graphics.Color;
//import android.os.Build;
//import android.util.Log;
//
//import androidx.annotation.NonNull;
//import androidx.core.app.NotificationCompat;
//
//import com.fasterxml.jackson.databind.ObjectMapper;
//import com.google.firebase.messaging.FirebaseMessagingService;
//import com.google.firebase.messaging.RemoteMessage;
//import com.nextsolutions.newfeed.R;
//import com.nextsolutions.newfeed.model.dto.NotificationDataDTO;
//import com.nextsolutions.newfeed.util.IntentConstants;
//
//public class MyFirebaseMessagingService extends FirebaseMessagingService {
//    @Override
//    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
//        super.onMessageReceived(remoteMessage);
//        Log.d("token_firebase", "From: " + remoteMessage.getFrom());
//        try {
//            // Check if message contains a notification payload.
//            final ObjectMapper mapper = new ObjectMapper(); // jackson's objectmapper
//            final NotificationDataDTO notificationDTO = mapper.convertValue(remoteMessage.getData(), NotificationDataDTO.class);
//            if (notificationDTO != null) {
//                sendNotification(notificationDTO);
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    private void sendNotification(NotificationDataDTO dataDTO) {
//        Intent notificationIntent = new Intent(dataDTO.getClick_action());
//        notificationIntent.putExtra(IntentConstants.INTENT_ITEM_ID, "" + dataDTO.getItemId());
//        notificationIntent.putExtra(IntentConstants.INTENT_CLICK_ACTION, dataDTO.getClick_action());
//        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
//        //
//        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//        String NOTIFICATION_CHANNEL_ID = "news_feed_notification";
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            @SuppressLint("WrongConstant") NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "My Notifications", NotificationManager.IMPORTANCE_MAX);
//            // Configure the notification channel.
//            notificationChannel.setDescription(dataDTO.getMessage());
//            notificationChannel.enableLights(true);
//            notificationChannel.setLightColor(Color.RED);
//            notificationChannel.setVibrationPattern(new long[]{0, 1000, 500, 1000});
//            notificationChannel.enableVibration(true);
//            notificationManager.createNotificationChannel(notificationChannel);
//        }
//        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID);
//        notificationBuilder.setAutoCancel(true)
//                .setDefaults(Notification.DEFAULT_ALL)
//                .setWhen(System.currentTimeMillis())
//                .setSmallIcon(R.drawable.heart_on)
//                .setPriority(Notification.PRIORITY_HIGH)
//                .setContentTitle(dataDTO.getTitle())
//                .setContentIntent(pendingIntent)
//                .setContentText(dataDTO.getMessage());
//        notificationManager.notify(1, notificationBuilder.build());
//    }
//
//    @Override
//    public void onDeletedMessages() {
//        super.onDeletedMessages();
//    }
//
//    @Override
//    public void onMessageSent(@NonNull String s) {
//        super.onMessageSent(s);
//    }
//
//    @Override
//    public void onSendError(@NonNull String s, @NonNull Exception e) {
//        super.onSendError(s, e);
//    }
//
//    @Override
//    public void onNewToken(@NonNull String s) {
//        super.onNewToken(s);
//    }
//}
