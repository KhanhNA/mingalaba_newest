package com.nextsolutions.newfeed.adapter;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.nextsolutions.newfeed.R;
import com.nextsolutions.newfeed.model.PeopleModel;
import com.nextsolutions.newfeed.util.IntentConstants;
import com.nextsolutions.newfeed.view.activity.ProfileActivity;

import java.util.List;

public class PeopleAdapter extends RecyclerView.Adapter<PeopleAdapter.PeopleViewHolder> {
    private Activity activity;
    private List<PeopleModel> dataSearch;

    public PeopleAdapter(Activity activity, List<PeopleModel> datas) {
        this.activity = activity;
        this.dataSearch = datas;
    }

    @NonNull
    @Override
    public PeopleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_search, parent, false);
        return new PeopleViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull PeopleViewHolder holder, int position) {
        PeopleModel searchUser = dataSearch.get(position);
        holder.name.setText(searchUser.getDisplayName());
        holder.email.setText(searchUser.getEmail());
        Glide.with(activity)
                .load(searchUser.getAvatar())
                .apply(RequestOptions.circleCropTransform())
                .into(holder.avartar);
        holder.itemView.setOnClickListener(v -> {
            Intent intent = new Intent(v.getContext(), ProfileActivity.class);
            intent.putExtra(IntentConstants.INTENT_USER_ID, dataSearch.get(position).getId());
            v.getContext().startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return dataSearch == null ? 0 : dataSearch.size();
    }

    public class PeopleViewHolder extends RecyclerView.ViewHolder {
        public TextView name;
        public TextView email;
        private ImageView avartar;

        private PeopleViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.txtName);
            email = view.findViewById(R.id.txtEmail);
            avartar = view.findViewById(R.id.imbAvartarSearch);
        }
    }


}
