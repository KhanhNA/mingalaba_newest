package com.nextsolutions.newfeed.listener;

import com.nextsolutions.newfeed.model.PeopleModel;

public interface LoginCallback {
    void loginSuccess(PeopleModel userData);

    void loginError(String message);
}
