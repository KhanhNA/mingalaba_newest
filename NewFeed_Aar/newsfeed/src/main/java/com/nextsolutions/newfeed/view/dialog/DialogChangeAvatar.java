package com.nextsolutions.newfeed.view.dialog;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.animators.AnimationType;
import com.luck.picture.lib.config.PictureConfig;
import com.luck.picture.lib.config.PictureMimeType;
import com.luck.picture.lib.entity.LocalMedia;
import com.luck.picture.lib.listener.OnResultCallbackListener;
import com.nextsolutions.UpdateUserMutation;
import com.nextsolutions.UserQuery;
import com.nextsolutions.newfeed.R;
import com.nextsolutions.newfeed.application.NewsFeedApplication;
import com.nextsolutions.newfeed.databinding.DialogChangeAvatarBinding;
import com.nextsolutions.newfeed.listener.RefreshListener;
import com.nextsolutions.newfeed.util.TsFileUtils;
import com.nextsolutions.newfeed.util.TsUtils;
import com.nextsolutions.newfeed.widget.glide.GlideEngine;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import io.reactivex.schedulers.Schedulers;

public class DialogChangeAvatar extends BottomSheetDialogFragment {

    private TsFileUtils tsFileUtils;
    private String username;
    private Integer type;
    private DialogChangeAvatarBinding binding;
    private RefreshListener refreshListener;
    private UserQuery.User user;

    // Type: 1 : Banner ; 2:Avatar
    public DialogChangeAvatar(UserQuery.User user, int type, RefreshListener listener) {
        this.username = user.username();
        this.type = type;
        this.refreshListener = listener;
        this.user = user;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_change_avatar, container, false);
        binding.txtViewImage.setOnClickListener(v -> {
            previewImage(type == 1 ? user.banner_url() : user.avatar());
        });
        binding.txtUploadImage.setText(type == 1 ? getString(R.string.change_image_banner) : getString(R.string.change_image_avatar));
        binding.txtUploadImage.setOnClickListener(v -> {
            openGallery();
        });
        binding.imgCancel.setOnClickListener(v -> dismiss());
        return binding.getRoot();
    }

    private void updateUserInfo(String url) {
        UpdateUserMutation updateUser;
        if (type == 1) {
            updateUser = UpdateUserMutation.builder()
                    .banner_url(url)
                    .username(username).build();
        } else {
            updateUser = UpdateUserMutation.builder()
                    .avatar_url(url)
                    .username(username).build();
        }
        NewsFeedApplication.getInstance().getApi().mutate(updateUser).enqueue(new ApolloCall.Callback<UpdateUserMutation.Data>() {
            @Override
            public void onResponse(@NotNull Response<UpdateUserMutation.Data> response) {
                Log.d("Upload", "Success");
                updateUI();
                NewsFeedApplication.getCurrentUser().setAvatar(url);
                refreshListener.onRefreshUi(type, url);
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {
                Log.d("Upload", "Error: " + e.getMessage());
                updateUI();
            }
        });
    }

    private void updateUI() {
        requireActivity().runOnUiThread(() -> {
            binding.layoutLoading.setVisibility(View.GONE);
            binding.layoutRoot.setVisibility(View.VISIBLE);
        });
    }

    private void previewImage(String url) {
        List<LocalMedia> lstMedia = new ArrayList<>();
        LocalMedia localMedia = new LocalMedia();
        localMedia.setPath(url);

        lstMedia.add(localMedia);

        PictureSelector.create(this)
                .themeStyle(R.style.picture_default_style)
                .isNotPreviewDownload(true)
                .loadImageEngine(GlideEngine.createGlideEngine())
                .openExternalPreview(0, lstMedia);
    }

    private void openGallery() {
        tsFileUtils = new TsFileUtils();
        PictureSelector.create(this)
                .openGallery(PictureMimeType.ofAll())
                .loadImageEngine(GlideEngine.createGlideEngine())
                .theme(R.style.picture_WeChat_style)
                .isWeChatStyle(true)
                .maxSelectNum(1)
                .setRecyclerAnimationMode(AnimationType.SLIDE_IN_BOTTOM_ANIMATION)
                .synOrAsy(true)
                .compress(true)// 是否压缩
                .cutOutQuality(85)
                .enableCrop(true)
                .circleDimmedLayer(true)
                .compressQuality(80)
                .selectionMode(PictureConfig.MULTIPLE)
                .forResult(new OnResultCallbackListener<LocalMedia>() {
                    @Override
                    public void onResult(List<LocalMedia> result) {
                        if (TsUtils.isNotNull(result)) {
                            // onResult Callback
                            binding.layoutLoading.setVisibility(View.VISIBLE);
                            File file = new File(Objects.requireNonNull(result.get(0).getCutPath()));
                            HashMap<File, String> mapFile = new HashMap<>();
                            mapFile.put(file, file.getName());
                            tsFileUtils.uploadMultiple(mapFile)
                                    .subscribeOn(Schedulers.io())
                                    .observeOn(Schedulers.io())
                                    .subscribe(() -> {
                                        String url = NewsFeedApplication.BASE_URL_FILE + file.getName();
                                        Log.d("Upload", "Uploads completed!: " + url);
                                        updateUserInfo(url);
                                    });
                        }

                    }

                    @Override
                    public void onCancel() {
                        // onCancel Callback
                    }
                });
    }

}
