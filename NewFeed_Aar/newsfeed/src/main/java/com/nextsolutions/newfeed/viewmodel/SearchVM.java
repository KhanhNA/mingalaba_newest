package com.nextsolutions.newfeed.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableBoolean;

import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;
import com.nextsolutions.SearchUserQuery;
import com.nextsolutions.newfeed.application.NewsFeedApplication;
import com.nextsolutions.newfeed.model.PeopleModel;
import com.nextsolutions.newfeed.util.IntentConstants;
import com.nextsolutions.newfeed.util.TsUtils;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.exceptionHandle.AppException;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class SearchVM extends BaseViewModel {
    public ArrayList<PeopleModel> lstUser = new ArrayList<>();
    public ObservableBoolean isShowNoData = new ObservableBoolean(true);
    public ObservableBoolean isLoading = new ObservableBoolean();

    public SearchVM(@NonNull Application application) {
        super(application);
    }

    public void search(String txtSearch, int page) {
        isLoading.set(true);
        NewsFeedApplication.getInstance().getApi().query(SearchUserQuery.builder().searchText(txtSearch).page_number(page).build()).enqueue(new ApolloCall.Callback<SearchUserQuery.Data>() {
            @Override
            public void onResponse(@NotNull Response<SearchUserQuery.Data> response) {
                SearchUserQuery.Data data = response.data();
                lstUser.clear();
                if (data != null) {
                    List<SearchUserQuery.SearchUser> searchUsers = data.searchUser();
                    if (searchUsers != null && searchUsers.size() > 0) {
                        lstUser.addAll(convertData(searchUsers));
                    }
                }
                try {
                    view.action(IntentConstants.SEARCH_SUCCESS, null, null, null);
                } catch (AppException e) {
                    e.printStackTrace();
                }
                isLoading.set(false);
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {
                isLoading.set(false);
            }
        });
    }

    private List<PeopleModel> convertData(List<SearchUserQuery.SearchUser> items) {
        if (items != null) {
            List<PeopleModel> result = new ArrayList<>();
            for (SearchUserQuery.SearchUser item : items) {
                PeopleModel post = new PeopleModel();
                post.setData(item);
                result.add(post);
            }
            return result;
        }
        return null;
    }
}
