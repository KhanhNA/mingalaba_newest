package com.nextsolutions.newfeed.listener;

import android.view.View;

public interface OnItemClickListener {
    void onItemClick(View v, int position);
}
