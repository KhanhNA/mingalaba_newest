package com.nextsolutions.newfeed.view.dialog;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.view.menu.MenuBuilder;
import androidx.appcompat.view.menu.MenuPopupHelper;
import androidx.appcompat.widget.PopupMenu;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.animators.AnimationType;
import com.luck.picture.lib.config.PictureConfig;
import com.luck.picture.lib.config.PictureMimeType;
import com.luck.picture.lib.decoration.GridSpacingItemDecoration;
import com.luck.picture.lib.entity.LocalMedia;
import com.luck.picture.lib.tools.ScreenUtils;
import com.nextsolutions.newfeed.R;
import com.nextsolutions.newfeed.adapter.GridImageAdapter;
import com.nextsolutions.newfeed.application.NewsFeedApplication;
import com.nextsolutions.newfeed.databinding.DialogCommentBinding;
import com.nextsolutions.newfeed.listener.MyResultCallback;
import com.nextsolutions.newfeed.model.CommentModel;
import com.nextsolutions.newfeed.model.PostModel;
import com.nextsolutions.newfeed.util.IntentConstants;
import com.nextsolutions.newfeed.util.TsUtils;
import com.nextsolutions.newfeed.view.activity.PostDetailActivity;
import com.nextsolutions.newfeed.view.activity.ProfileActivity;
import com.nextsolutions.newfeed.view.adapter.CommentAdapter;
import com.nextsolutions.newfeed.viewmodel.CommentVM;
import com.nextsolutions.newfeed.widget.FullyGridLayoutManager;
import com.nextsolutions.newfeed.widget.glide.GlideEngine;
import com.tsolution.base.listener.AdapterListener;

import java.util.ArrayList;
import java.util.List;

public class DialogComment extends BottomSheetDialogFragment {
    private CommentVM commentVM;
    private PostModel post;
    private CommentAdapter commentAdapter;
    private int userCommentCount;
    private OnDismiss onDismiss;
    private GridImageAdapter imageSelectAdapter;
    private DialogCommentBinding binding;

    DialogConfirm dialogDelete;

    public DialogComment(PostModel post, OnDismiss onDismiss) {
        this.post = post;
        this.onDismiss = onDismiss;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.DialogStyle);
    }

    @SuppressLint("ClickableViewAccessibility")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        commentVM = ViewModelProviders.of(this).get(CommentVM.class);
        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_comment, container, false);
        binding.setViewModel(commentVM);
        initAdapter(savedInstanceState);
        commentVM.getPost().set(post);
        commentVM.getPostById();

        commentAdapter = new CommentAdapter(commentVM.getListComment().getValue(), new AdapterListener() {
            @Override
            public void onItemClick(View view, Object o) {
                int id = view.getId();
                if (id == R.id.imgAuthor || id == R.id.txtAuthorName) {
                    Intent intent = new Intent(requireActivity(), ProfileActivity.class);
                    intent.putExtra(IntentConstants.INTENT_USER_ID, ((CommentModel) o).getUser_id());
                    startActivity(intent);
                } else if (id == R.id.seeMoreComment) {
                    view.findViewById(R.id.txtSeeMore).setVisibility(View.GONE);
                    view.findViewById(R.id.loadingSeeMore).setVisibility(View.VISIBLE);
                    commentVM.getMoreComment(post.getId());
                }
            }

            @Override
            public void onItemLongClick(View view, Object o) {
                if (view.getId() == R.id.txtComment) {
                    showCommentOption(view, (CommentModel) o);
                }
            }
        });

        LinearLayoutManager layoutManager = new LinearLayoutManager(requireActivity(), RecyclerView.VERTICAL, false);
        binding.rcComment.setLayoutManager(layoutManager);
        binding.rcComment.setAdapter(commentAdapter);

        commentVM.getComments(post.getId(), 0);
       
        registerObserver();

        binding.btnAddImg.setOnClickListener(v -> pickImagesForComment());
        binding.txtAddComment.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                boolean enableAddComment = s.length() > 0 || commentVM.getIsNotEmpty().get();
                binding.btnAddComment.setVisibility(enableAddComment ? View.VISIBLE : View.GONE);
                if (s.length() > 1000) {
                    binding.txtAddComment.setError(getString(R.string.text_too_long));
                    binding.btnAddComment.setEnabled(false);
                } else {
                    binding.txtAddComment.setError(null);
                    binding.btnAddComment.setEnabled(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        //xử lý sự kiện cuộn
        binding.txtAddComment.setOnTouchListener((v, event) -> {
            v.getParent().requestDisallowInterceptTouchEvent(true);
            if ((event.getAction() & MotionEvent.ACTION_MASK) == MotionEvent.ACTION_UP) {
                v.getParent().requestDisallowInterceptTouchEvent(false);
            }
            return false;
        });

        binding.txtCancelEdit.setOnClickListener(v -> {
            commentVM.getIsEdit().set(false);
            commentVM.getCmtObs().set(new CommentModel());
            commentVM.setDataFileChoose(new ArrayList<>());
        });

        binding.btnAddComment.setOnClickListener(v -> {
            CommentModel commentModel = commentVM.getCmtObs().get();
            assert commentModel != null;

            //nếu là edit comment
            if (commentVM.getIsEdit().get()) {
                requireActivity().runOnUiThread(() -> {
                    commentVM.getListComment().getValue().set(commentModel.index - 1, commentModel);
                    commentAdapter.notifyItemChanged(commentModel.index - 1);
                    binding.rcComment.smoothScrollToPosition(commentModel.index - 1);
                });
            } else {//add comment
                userCommentCount++;
                commentModel.setUser_id(NewsFeedApplication.getCurrentUserId());
                commentModel.setType(1);
                commentModel.setUsername(NewsFeedApplication.getCurrentUser().getUsername());
                commentModel.setDisplay_name(NewsFeedApplication.getCurrentUser().getDisplay_name());
                commentModel.setUser_avatar_url(NewsFeedApplication.getCurrentUser().getAvatar());
               // commentModel.setUrl();
                commentVM.getListComment().getValue().add(0, commentModel);
                requireActivity().runOnUiThread(() -> {
                    commentAdapter.notifyItemInserted(0);
                    binding.rcComment.smoothScrollToPosition(0);
                    binding.emptyComment.setVisibility(View.GONE);
                });
            }
            commentModel.setMessage(binding.txtAddComment.getText().toString());
            commentVM.upLoadComment(post.getId());
            binding.txtAddComment.clearFocus();
            binding.txtAddComment.setText("");

        });
        return binding.getRoot();
    }

    private void registerObserver() {
        commentVM.getClearData().observe(this, isClear -> {
            if (isClear)
                imageSelectAdapter.clearData();
        });

        commentVM.getNotifyChangePosition().observe(this, position->{
            if(position >= 0) {
                commentAdapter.notifyItemChanged(position);
            }
        });

        commentVM.getListComment().observe(this, list -> {
            commentAdapter.notifyDataSetChanged();
            if (list.size() > 0) {
                binding.emptyComment.setVisibility(View.GONE);
            } else {
                binding.emptyComment.setVisibility(View.VISIBLE);
            }
        });
    }

    @SuppressLint("RestrictedApi")
    private void showCommentOption(View view, CommentModel commentModel) {
        //Creating the instance of PopupMenu
        PopupMenu popup = new PopupMenu(getActivity(), view);
        //Inflating the Popup using xml file
        if (commentModel.getUser_id().equals(NewsFeedApplication.getCurrentUserId())) {
            popup.getMenuInflater().inflate(R.menu.menu_option_my_comment, popup.getMenu());
        } else {
            popup.getMenuInflater().inflate(R.menu.menu_option_comment, popup.getMenu());
        }

        MenuPopupHelper menuHelper = new MenuPopupHelper(getActivity(), (MenuBuilder) popup.getMenu(), view);
        menuHelper.setForceShowIcon(true);

        popup.setOnMenuItemClickListener(item -> {
            int itemId = item.getItemId();
            if (itemId == R.id.action_coppy) {
                ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("Good_Boy", commentModel.getMessage());
                clipboard.setPrimaryClip(clip);
                Toast.makeText(getActivity(), getResources().getString(R.string.COPIED_TO_CLIPBOARD), Toast.LENGTH_SHORT).show();
            } else if (itemId == R.id.action_edit) {
                openEditComment(commentModel);
            } else if (itemId == R.id.action_del) {
                showDialogDelete(commentModel);
            }
            return true;
        });

        menuHelper.show();//showing popup menu
    }

    private void openEditComment(CommentModel commentModel) {
        commentVM.getIsEdit().set(true);
        commentVM.getCmtObs().set(commentModel);
        if (TsUtils.isNotNull(commentModel.getUrl())) {
            List<LocalMedia> localMediaList = new ArrayList<>();
            for (String url : commentModel.getUrl()) {
                LocalMedia localMedia = new LocalMedia();
                localMedia.setPath(url);
                localMediaList.add(localMedia);
            }
            commentVM.setDataFileChoose(localMediaList);
            imageSelectAdapter.setList(localMediaList);
        }
    }

    private void showDialogDelete(CommentModel commentModel) {
        dialogDelete = new DialogConfirm(getString(R.string.delete_comment), "", v -> {
            commentVM.deleteComment(commentModel);
            commentVM.getListComment().getValue().remove(commentModel.index - 1);
            commentAdapter.notifyDataSetChanged();
            dialogDelete.dismiss();
        });
        dialogDelete.show(getChildFragmentManager(), dialogDelete.getTag());
    }

    private void initLayoutManager() {
        FullyGridLayoutManager manager = new FullyGridLayoutManager(requireActivity(),
                4, GridLayoutManager.VERTICAL, false);
        binding.rcImages.setLayoutManager(manager);

        binding.rcImages.addItemDecoration(new GridSpacingItemDecoration(4,
                ScreenUtils.dip2px(requireActivity(), 8), false));
    }

    private void initAdapter(Bundle savedInstanceState) {
        initLayoutManager();
        imageSelectAdapter = new GridImageAdapter(requireActivity(), this::pickImage);
        if (savedInstanceState != null && savedInstanceState.getParcelableArrayList("selectorList") != null) {
            imageSelectAdapter.setList(savedInstanceState.getParcelableArrayList("selectorList"));
        }
        imageSelectAdapter.setSelectMax(6);
        imageSelectAdapter.setOnClear(() -> commentVM.getIsNotEmpty().set(false));
        binding.rcImages.setAdapter(imageSelectAdapter);
        imageSelectAdapter.setOnItemClickListener((v, position) -> {
            openOptionImage(position);
        });
    }

    private void openOptionImage(int position) {
        List<LocalMedia> selectList = imageSelectAdapter.getData();
        if (selectList.size() > 0) {
            LocalMedia media = selectList.get(position);
            String mimeType = media.getMimeType();
            int mediaType = PictureMimeType.getMimeType(mimeType);
            switch (mediaType) {
                case PictureConfig.TYPE_VIDEO:
                    PictureSelector.create(requireActivity())
                            .themeStyle(R.style.picture_default_style)
                            .externalPictureVideo(TextUtils.isEmpty(media.getAndroidQToPath()) ? media.getPath() : media.getAndroidQToPath());
                    break;
                default:
                    PictureSelector.create(requireActivity())
                            .themeStyle(R.style.picture_default_style)
                            .setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED)
                            .isNotPreviewDownload(true)
                            .loadImageEngine(GlideEngine.createGlideEngine())
                            .openExternalPreview(position, selectList);
                    break;
            }
        }
    }


    private void pickImagesForComment() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (requireActivity().checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                    && requireActivity().checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                    && requireActivity().checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                pickImage();
            } else {
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, PostDetailActivity.PICK_IMAGE_PERMISSIONS_REQUEST_CODE);
            }
        } else {
            pickImage();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PostDetailActivity.PICK_IMAGE_PERMISSIONS_REQUEST_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                pickImage();
            } else {
                Toast.makeText(requireActivity(), R.string.permissions_not_granted, Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void pickImage() {
        PictureSelector.create(this)
                .openGallery(1)
                .loadImageEngine(GlideEngine.createGlideEngine())
                .theme(R.style.picture_WeChat_style)
                .isWeChatStyle(true)
                .maxSelectNum(6)
                .setRecyclerAnimationMode(AnimationType.SLIDE_IN_BOTTOM_ANIMATION)
                .synOrAsy(true)
                .compress(true)
                .compressQuality(80)
                .selectionMode(PictureConfig.MULTIPLE)
                .selectionMedia(imageSelectAdapter.getData())
                .forResult(new MyResultCallback(imageSelectAdapter, commentVM));
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        BottomSheetDialog dialog = (BottomSheetDialog) getDialog();
        FrameLayout bottomSheet = dialog.findViewById(com.google.android.material.R.id.design_bottom_sheet);
        BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);
        behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View view, int i) {
                if(i == BottomSheetBehavior.STATE_COLLAPSED){
                    behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                }
                if(i == BottomSheetBehavior.STATE_HIDDEN){
                    dismiss();
                }
            }

            @Override
            public void onSlide(@NonNull View view, float v) {

            }
        });
//        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
    }

    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        onDismiss.onDismiss(userCommentCount);
        super.onDismiss(dialog);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        commentVM.getListComment().removeObservers(this);
        commentVM.getClearData().removeObservers(this);
        commentVM.getNotifyChangePosition().removeObservers(this);
    }

    public interface OnDismiss {
        void onDismiss(Integer commentCount);
    }

}
