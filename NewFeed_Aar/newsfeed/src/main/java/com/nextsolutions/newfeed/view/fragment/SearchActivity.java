package com.nextsolutions.newfeed.view.fragment;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.provider.SearchRecentSuggestions;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;

import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.nextsolutions.newfeed.R;
import com.nextsolutions.newfeed.adapter.PeopleAdapter;
import com.nextsolutions.newfeed.databinding.FragmentSearchBinding;
import com.nextsolutions.newfeed.util.IntentConstants;
import com.nextsolutions.newfeed.viewmodel.SearchVM;
import com.nextsolutions.newfeed.widget.MySuggestionProvider;
import com.tsolution.base.BaseActivity;
import com.tsolution.base.BaseViewModel;

public class

SearchActivity extends BaseActivity {
    private SearchVM searchVM;
    private FragmentSearchBinding mBinding;
    private PeopleAdapter searchAdapter;
    private String preText = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = (FragmentSearchBinding) binding;
        searchVM = (SearchVM) viewModel;

        searchAdapter = new PeopleAdapter(getBaseActivity(), searchVM.lstUser);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        mBinding.rcSearchPeople.setLayoutManager(mLayoutManager);
        mBinding.rcSearchPeople.setAdapter(searchAdapter);

        mBinding.searchBarSearchView.onActionViewExpanded();
        mBinding.searchBarSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                if (!newText.isEmpty()) {
                    if(!preText.equals(newText)) {
                        searchVM.isLoading.set(true);
                        new Handler().postDelayed(() -> {
                            preText = newText;
                            searchVM.search(newText, 0);
                        }, 1000);
                    }
                } else {
                    searchVM.lstUser.clear();
                    searchAdapter.notifyDataSetChanged();
                }
                return true;
            }
        });

        mBinding.refresh.setOnRefreshListener(() -> {
            searchVM.search(preText, 0);
        });

        mBinding.imgBack.setOnClickListener((v) -> finish());
    }

    @Override
    public void onBackPressed() {
        // close search view on back button pressed
        super.onBackPressed();
    }

    @Override
    public void processFromVM(String action, View view, BaseViewModel viewModel, Throwable t) {
        if (action.equals(IntentConstants.SEARCH_SUCCESS)) {
            runOnUiThread(() -> {
                searchAdapter.notifyDataSetChanged();
                if (searchVM.lstUser.size() > 0) {
                    searchVM.isShowNoData.set(false);
                } else {
                    searchVM.isShowNoData.set(true);
                }
            });

        }
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_search;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return SearchVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }
}
