package com.nextsolutions.newfeed.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.entity.LocalMedia;
import com.luck.picture.lib.listener.OnItemClickListener;
import com.nextsolutions.newfeed.R;
import com.nextsolutions.newfeed.util.TsUtils;
import com.nextsolutions.newfeed.widget.glide.GlideEngine;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

public class ImageAdapter extends
        RecyclerView.Adapter<ImageAdapter.ViewHolder> {
    private static final int TYPE_CAMERA = 1;
    private static final int TYPE_PICTURE = 2;
    private LayoutInflater mInflater;
    private List<String> list;
    private int size;
    private Context context;
    private List<LocalMedia> mediaList;

    public ImageAdapter(Context context, List<String> list, int size) {
        this.mInflater = LayoutInflater.from(context);
        this.list = list;
        this.size = size;
        this.context = context;
        mediaList = new ArrayList<>();
        if (TsUtils.isNotNull(list)) {
            for (String s : list) {
                LocalMedia localMedia = new LocalMedia();
                localMedia.setPath(s);
                mediaList.add(localMedia);
            }
        }

    }


    public List<String> getData() {
        return list == null ? new ArrayList<>() : list;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView mImg;
        TextView txtSeeMore;

        public ViewHolder(View view) {
            super(view);
            mImg = view.findViewById(R.id.fiv);
            txtSeeMore = view.findViewById(R.id.txtSeeMore);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (isShowMoreItem(position)) {
            return TYPE_CAMERA;
        } else {
            return TYPE_PICTURE;
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = mInflater.inflate(R.layout.item_image,
                viewGroup, false);
        return new ViewHolder(view);
    }

    private boolean isShowMoreItem(int position) {
        int size = list.size() == 0 ? 0 : list.size();
        return position == size - 1 && this.size > 5;
    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {
        if (getItemViewType(position) == TYPE_CAMERA) {
            Glide.with(viewHolder.itemView.getContext())
                    .load(list.get(position))
                    .placeholder(R.drawable.ic_stub)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(viewHolder.mImg);
            viewHolder.txtSeeMore.setVisibility(View.VISIBLE);
            viewHolder.txtSeeMore.setText("+" + (size - 5));

        } else {
            String path = list.get(position);
            if (TextUtils.isEmpty(path)) {
                return;
            }
            Glide.with(viewHolder.itemView.getContext())
                    .load(path)
                    .placeholder(R.drawable.ic_stub)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(viewHolder.mImg);
            viewHolder.itemView.setOnClickListener(v -> {
                previewImage(viewHolder.getAdapterPosition());
            });
        }
    }

    private OnItemClickListener mItemClickListener;

    public void setOnItemClickListener(OnItemClickListener l) {
        this.mItemClickListener = l;
    }

    private void previewImage(int position) {
        try {
            PictureSelector.create(unwrap(context))
                    .themeStyle(R.style.picture_default_style)
                    .isNotPreviewDownload(true)
                    .loadImageEngine(GlideEngine.createGlideEngine())
                    .openExternalPreview(position, mediaList);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private static Activity unwrap(Context context) {
        while (!(context instanceof Activity) && context instanceof ContextWrapper) {
            context = ((ContextWrapper) context).getBaseContext();
        }

        return (Activity) context;
    }
}
