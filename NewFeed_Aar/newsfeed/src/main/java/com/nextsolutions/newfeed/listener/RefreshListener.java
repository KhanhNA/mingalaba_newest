package com.nextsolutions.newfeed.listener;

public interface RefreshListener {
    void onRefreshUi(Integer type, String url);
}
