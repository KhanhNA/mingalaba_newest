package com.nextsolutions.newfeed.util;

public class IntentConstants {
    public static final String FOLLOW_SUCCESS = "followSuccess";
    public static final String FOLLOW_FAIL = "followFail";
    public static final String UNFOLLOW_FAIL = "unFollowFail";
    public static final String UNFOLLOW_SUCCESS = "unFollowSuccess";
    public static final String chatAppId = "im.vector.app";
    public static final String SEARCH_SUCCESS = "searchSuccess";
    public static final String INTENT_SEARCH_PROFILE = "searchProfile";
    public static final String INTENT_USER_ID = "userId";
    public static final String GET_PROFILE_SUCCESS = "getProfileSuccess";
    public static final String INTENT_ITEM_ID = "itemId";
    public static final String INTENT_CLICK_ACTION = "click_action";
    public static final String DELETE_SUCCESS = "DELETE_SUCCESS";
}
