package com.nextsolutions.newfeed.util;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.entity.LocalMedia;
import com.luck.picture.lib.listener.OnItemClickListener;
import com.nextsolutions.newfeed.R;
import com.nextsolutions.newfeed.adapter.GridImageAdapter;
import com.nextsolutions.newfeed.adapter.ImageAdapter;
import com.nextsolutions.newfeed.widget.glide.GlideEngine;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import static android.view.View.VISIBLE;

public class BindingAdapterUtil {
    //CODE THEO KIỂU NÔNG DÂN
    @SuppressLint("SetTextI18n")
    @BindingAdapter("imagesPost")
    public static void viewImages(LinearLayout layout, List<String> attrs) {
        //Đoạn code tay to
        if (attrs != null && attrs.size() > 0) {
            layout.setVisibility(View.VISIBLE);
            TextView moreImage;
            ImageView[] imgs = new ImageView[5];
            int[] imgsId = {R.id.image1, R.id.image2, R.id.image3, R.id.image4, R.id.image5};

            final View rootView =
                    LayoutInflater.from(layout.getContext()).inflate(R.layout.view_post_images, layout, true);
            for (int i = 0; i < 5; i++) {
                imgs[i] = rootView.findViewById(imgsId[i]);
            }
            moreImage = rootView.findViewById(R.id.moreImage);
            if (attrs.size() > 5) {
                moreImage.setText("+" + (attrs.size() - 5));
                moreImage.setVisibility(View.VISIBLE);
                for (int i = 0; i < 5; i++) {
                    imgs[i].setVisibility(View.VISIBLE);
                    if (attrs.get(i) != null && !attrs.get(i).equals("")) {
                        loadImage(imgs[i], attrs.get(i));
                        int finalI = i;
                        imgs[i].setOnClickListener(v -> previewImage(imgs[0].getContext(), attrs, finalI));
                    }
                }
            } else {
                switch (attrs.size()) {
                    case 1:
                        hideAndShowImage(imgs, attrs, 0);
                        break;
                    case 2:
                        hideAndShowImage(imgs, attrs, 0, 1);
                        break;
                    case 3:
                        hideAndShowImage(imgs, attrs, 0, 2, 3);
                        break;
                    case 4:
                        hideAndShowImage(imgs, attrs, 0, 1, 2, 3);
                        break;
                    case 5:
                        hideAndShowImage(imgs, attrs, 0, 1, 2, 3, 4);
                        break;
                }
                moreImage.setVisibility(View.GONE);
            }
        } else {
            layout.setVisibility(View.GONE);
        }
    }

    private static void hideAndShowImage(ImageView[] imgs, List<String> attrs, Integer... position) {
        int j = 0;
        for (int i = 0; i < 5; i++) {

            if (j < position.length && i == position[j]) {
                imgs[i].setVisibility(View.VISIBLE);
                int finalJ = j;
                imgs[i].setOnClickListener(v -> previewImage(imgs[0].getContext(), attrs, finalJ));
                loadImage(imgs[i], attrs.get(j));
                j++;
            } else {
                imgs[i].setVisibility(View.GONE);
            }
        }
    }

    private static void previewImage(Context context, List<String> list, int position) {
        List<LocalMedia> mediaList = new ArrayList<>();
        if (TsUtils.isNotNull(list)) {
            for (String s : list) {
                LocalMedia localMedia = new LocalMedia();
                localMedia.setPath(s);
                mediaList.add(localMedia);
            }
        }
        try {
            PictureSelector.create(unwrap(context))
                    .themeStyle(R.style.picture_default_style)
                    .isNotPreviewDownload(true)
                    .loadImageEngine(GlideEngine.createGlideEngine())
                    .openExternalPreview(position, mediaList);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private static Activity unwrap(Context context) {
        while (!(context instanceof Activity) && context instanceof ContextWrapper) {
            context = ((ContextWrapper) context).getBaseContext();
        }

        return (Activity) context;
    }

    @BindingAdapter("imagesPost")
    public static void viewImages(RecyclerView layout, List<String> attrs) {
        if (attrs == null || attrs.size() == 0) {
            layout.setVisibility(View.GONE);
            return;
        }
        int size = attrs.size();
        if (size > 5) {
            attrs = attrs.subList(0, 5);
        }
        ImageAdapter imageAdapter = new ImageAdapter(layout.getContext(), attrs, size);
        layout.setAdapter(imageAdapter);
        final GridLayoutManager manager = new GridLayoutManager(layout.getContext(), 6);

        //clone list to use anonymous function
        List<String> finalAttrs = attrs;
        manager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                int index = position % 5;
                switch (finalAttrs.size()) {
                    case 1:
                        return 6;
                    case 2:
                    case 4:
                        return 3;
                    case 3:
                        if (index == 0) return 6;
                        return 3;
                    case 5:
                        switch (index) {
                            case 0:
                            case 1:
                                return 3;
                            case 2:
                            case 3:
                            case 4:
                                return 2;
                        }
                }
                return 0;
            }
        });
        layout.setLayoutManager(manager);
        imageAdapter.notifyDataSetChanged();
    }


    private static void loadImage(ImageView imageView, String url) {
        Glide.with(imageView.getContext())
                .load(url)
                .apply(new RequestOptions().override(500, 500))
                .error(R.drawable.ic_stub)
                .into(imageView);
    }

    @BindingAdapter("merchantUrl")
    public static void merchantUrl(ImageView view, String imageUrl) {
        Glide.with(view.getContext())
                .load(imageUrl)
                .apply(new RequestOptions().override(200, 200))
                .error(R.drawable.ic_stub)
                .into(view);
    }

    @BindingAdapter("timer")
    public static void calculatorTime(TextView view, Date time) {
        if (time != null) {
            if ((System.currentTimeMillis() - time.getTime()) < 60 * 1000) {
                view.setText(R.string.now);
            } else {
                view.setText(DateUtils.getRelativeDateTimeString(
                        view.getContext(), // Suppose you are in an activity or other Context subclass
                        time.getTime(), // The time to display
                        DateUtils.MINUTE_IN_MILLIS, // The resolution. This will display only
                        DateUtils.WEEK_IN_MILLIS, // The maximum resolution at which the time will switch
                        0));
            }
        } else {
            view.setText(R.string.now);
        }
    }

    @BindingAdapter({"setFont", "setColor"})
    public static void fontColorText(TextView view, String font, String color) {
        if (font != null && !font.isEmpty()) {
            try {
                Typeface type = Typeface.createFromAsset(view.getContext().getAssets(), "fonts/" + font);
                view.setTypeface(type);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            view.setTypeface(null, Typeface.BOLD);
        }
        if (color != null && !color.isEmpty()) {
            try {
                view.setTextColor(Color.parseColor(color));
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            view.setTextColor(Color.parseColor("#24282C"));
        }
    }

}
