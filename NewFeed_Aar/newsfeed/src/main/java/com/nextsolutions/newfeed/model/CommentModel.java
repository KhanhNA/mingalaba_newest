package com.nextsolutions.newfeed.model;

import android.view.View;

import com.nextsolutions.newfeed.widget.ExpandableTextView;
import com.tsolution.base.BaseModel;
import com.tsolution.base.listener.AdapterListener;

import java.util.Date;
import java.util.List;

import androidx.databinding.BindingAdapter;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CommentModel extends BaseModel {
    Integer id;
    Integer user_id;
    String user_avatar_url;
    String username;
    Boolean islike;
    int like;
    int type;
    String message;
    List<String> url;
    String description;
    Date createdAt;
    Date updatedAt;
    Integer comment_number;
    Integer total;
    String display_name;
    public String getDisplayName() {
        return display_name != null && !display_name.isEmpty() ? display_name : username;
    }

    @BindingAdapter({"longClick", "object"})
    public static void onLongClick(View v, AdapterListener adapterListener, Object o) {
       v.setOnLongClickListener(v1 -> {
           adapterListener.onItemLongClick(v1, o);
           return false;
       });
    }
}
