package com.nextsolutions.newfeed.util;

import androidx.databinding.InverseMethod;

public class Boxer {

    public static final char DEAULT_CHAR = ' ';
    private static final int DEFAULT_NUMBER = 0;

    @InverseMethod("boxBoolean")
    public static boolean unbox(Boolean b) {
        return (b != null) && b;
    }

    public static Boolean boxBoolean(boolean b) {
        return b;
    }

    @InverseMethod("boxChar")
    public static char unbox(Character c) {
        return c != null ? c : DEAULT_CHAR;
    }

    public static Character boxChar(char c) {
        return c;
    }

    @InverseMethod("boxByte")
    public static byte unbox(Byte b) {
        return b != null ? b : DEFAULT_NUMBER;
    }

    public static Byte boxByte(byte b) {
        return b;
    }

    @InverseMethod("boxShort")
    public static short unbox(Short s) {
        return s != null ? s : DEFAULT_NUMBER;
    }

    public static Short boxShort(short s) {
        return s;
    }

    @InverseMethod("boxInteger")
    public static int unbox(Integer i) {
        return i != null ? i : DEFAULT_NUMBER;
    }

    public static Integer boxInteger(int i) {
        return i;
    }

    @InverseMethod("boxInteger")
    public static long unbox(Long l) {
        return l != null ? l : DEFAULT_NUMBER;
    }

    public static Long boxLong(long l) {
        return l;
    }

    @InverseMethod("boxFloat")
    public static float unbox(Float f) {
        return f != null ? f : DEFAULT_NUMBER;
    }

    public static Float boxFloat(float f) {
        return f;
    }

    @InverseMethod("boxDouble")
    public static double unbox(Double d) {
        return (d != null) ? d : DEFAULT_NUMBER;
    }

    public static Double boxDouble(double d) {
        return d;
    }


}