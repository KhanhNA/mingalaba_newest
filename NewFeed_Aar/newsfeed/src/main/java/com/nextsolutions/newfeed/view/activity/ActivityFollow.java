package com.nextsolutions.newfeed.view.activity;

import android.os.Bundle;
import android.view.View;

import com.nextsolutions.newfeed.R;
import com.nextsolutions.newfeed.adapter.FollowPagerAdapter;
import com.nextsolutions.newfeed.databinding.ActivityFollowBinding;
import com.nextsolutions.newfeed.util.IntentConstants;
import com.tsolution.base.BaseActivity;
import com.tsolution.base.BaseViewModel;

public class ActivityFollow extends BaseActivity<ActivityFollowBinding> {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding.tabLayout.setupWithViewPager(binding.pager);
        String[] title = {getString(R.string.followings), getString(R.string.followers)};
        int userId = 0;
        if (getIntent() != null && getIntent().getExtras() != null) {
            if (getIntent().getExtras().containsKey(IntentConstants.INTENT_USER_ID)) {
                userId = getIntent().getExtras().getInt(IntentConstants.INTENT_USER_ID, 0);
            }
        }
        FollowPagerAdapter followPagerAdapter = new FollowPagerAdapter(getSupportFragmentManager(), userId, title);
        binding.pager.setAdapter(followPagerAdapter);
        if (getIntent() != null && getIntent().getExtras() != null && getIntent().getExtras().containsKey("position")) {
            binding.pager.setCurrentItem(getIntent().getExtras().getInt("position", 0));
        }
        binding.imgBack.setOnClickListener(v -> finish());
    }

    @Override
    public void processFromVM(String action, View view, BaseViewModel viewModel, Throwable t) {
        super.processFromVM(action, view, viewModel, t);
    }

    @Override
    public int getLayoutRes() {
        return R.layout.activity_follow;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return BaseViewModel.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }
}