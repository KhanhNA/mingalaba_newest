package com.nextsolutions.newfeed.listener;


import android.view.View;

import androidx.annotation.IdRes;
import androidx.annotation.LayoutRes;

import com.tsolution.base.BaseViewModel;


public interface DefaultFunctionActivity {

    @LayoutRes
    int getLayoutRes();

    Class<? extends BaseViewModel> getVMClass();

    @IdRes
    int getRecycleResId();

    @IdRes
    default int getXRecycleResId() {
        return getRecycleResId();
    }


    default void processFromVM(String action, View view, BaseViewModel viewModel, Throwable t) {

    }


}
