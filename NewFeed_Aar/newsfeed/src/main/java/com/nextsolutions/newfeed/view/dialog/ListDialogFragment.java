package com.nextsolutions.newfeed.view.dialog;

import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.nextsolutions.newfeed.R;
import com.nextsolutions.newfeed.adapter.XBaseAdapter;
import com.nextsolutions.newfeed.databinding.FragmentSelectionListBinding;
import com.tsolution.base.listener.AdapterListener;

import java.util.List;

public class ListDialogFragment extends BottomSheetDialogFragment {
    private List lstData;
    private int itemLayout;
    private AdapterListener listener;
    private int title;

    public ListDialogFragment(@LayoutRes int itemLayout, @StringRes int title, List lstData, AdapterListener adapterListener) {
        this.lstData = lstData;
        this.itemLayout = itemLayout;
        this.listener = adapterListener;
        this.title = title;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        FragmentSelectionListBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_selection_list, container, false);
        binding.txtTitle.setText(getString(title));

        XBaseAdapter packingAdapter = new XBaseAdapter(itemLayout, lstData, listener);
        RecyclerView rcIncentive = binding.rcIncentiveProduct;
        rcIncentive.setAdapter(packingAdapter);
        rcIncentive.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.btnDismiss.setOnClickListener(v -> this.dismiss());
        DisplayMetrics displayMetrics = new DisplayMetrics();
        requireActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        binding.root.setMinHeight(height / 2);

        return binding.getRoot();
    }

}