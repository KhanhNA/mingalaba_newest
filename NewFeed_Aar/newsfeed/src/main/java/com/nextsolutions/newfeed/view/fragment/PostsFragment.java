package com.nextsolutions.newfeed.view.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SimpleItemAnimator;

import com.bumptech.glide.Glide;
import com.jcodecraeer.xrecyclerview.ProgressStyle;
import com.jcodecraeer.xrecyclerview.XRecyclerView;
import com.nextsolutions.newfeed.R;
import com.nextsolutions.newfeed.application.NewsFeedApplication;
import com.nextsolutions.newfeed.databinding.FragmentPostBinding;
import com.nextsolutions.newfeed.model.PostModel;
import com.nextsolutions.newfeed.util.IntentConstants;
import com.nextsolutions.newfeed.view.activity.AddPostActivity;
import com.nextsolutions.newfeed.view.activity.ProfileActivity;
import com.nextsolutions.newfeed.view.adapter.FeedAdapter;
import com.nextsolutions.newfeed.view.dialog.DialogComment;
import com.nextsolutions.newfeed.view.dialog.DialogConfirm;
import com.nextsolutions.newfeed.view.dialog.DialogPostOption;
import com.nextsolutions.newfeed.viewmodel.PostVM;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.listener.AdapterListener;

public class PostsFragment extends BaseCustomFragment {
    private int userId;
    private Boolean isLoadData = true;
    private DialogConfirm dialogConfirm;
    private boolean isHideSearchIcon;

    public PostsFragment(int userId) {
        this.userId = userId;
    }

    public PostsFragment() {

    }

    public PostsFragment(boolean isHideSearchIcon) {
        this.isHideSearchIcon = isHideSearchIcon;
    }

    private FragmentPostBinding mBinding;
    private PostVM postVM;
    private FeedAdapter postsAdapter;
    private static int REQUEST_ADD_POST = 3232;
    private static int REQUEST_EDIT_POST = 6262;
    private int itemIndex;

    @Override
    public void onResume() {
        super.onResume();
        try {

            if (isLoadData) {
                String urlAvatar = NewsFeedApplication.getCurrentUser().getAvatar() != null ? NewsFeedApplication.getCurrentUser().getAvatar() : "";
                Glide.with(requireActivity()).load(urlAvatar).placeholder(R.drawable.ic_stub).into(mBinding.imgAuthor);
                isLoadData = false;
                if (userId != 0) {
                    postVM.getPosts(userId, 0);
                } else {
                    postVM.getPostPublic(NewsFeedApplication.getCurrentUserId(), 0);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        postVM = (PostVM) viewModel;
        mBinding = (FragmentPostBinding) binding;
        try {
            initView();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return binding.getRoot();
    }

    private void initView() {

        if (isHideSearchIcon) {
            mBinding.icSearch.setVisibility(View.GONE);
        }

        if (userId != 0 && userId != NewsFeedApplication.getCurrentUserId()) {
            mBinding.llAuthor.setVisibility(View.GONE);
        } else {
            mBinding.llAuthor.setVisibility(View.VISIBLE);
        }

        mBinding.imgAuthor.setOnClickListener(v -> {
            Intent intentProfile = new Intent(getActivity(), ProfileActivity.class);
            intentProfile.putExtra(IntentConstants.INTENT_USER_ID, NewsFeedApplication.getCurrentUserId());
            requireActivity().startActivity(intentProfile);
        });
        mBinding.rcPosts.getDefaultRefreshHeaderView().setState(2);
        postsAdapter = new FeedAdapter(postVM.getListPost(), new AdapterListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onItemClick(View view, Object o) {
                if (view.getId() == R.id.imgAuthor || view.getId() == R.id.txtAuthorName) {
                    Intent intent = new Intent(requireActivity(), ProfileActivity.class);
                    intent.putExtra(IntentConstants.INTENT_USER_ID, ((PostModel) o).getUser_id());
                    requireActivity().startActivity(intent);
                } else if (view.getId() == R.id.btnLike) {
                    RecyclerView.ViewHolder viewHolder = mBinding.rcPosts.findViewHolderForAdapterPosition(((PostModel) o).index);
                    TextView txtLikeCount = viewHolder.itemView.findViewById(R.id.txtLikeCount);
                    if (((PostModel) o).getIslike()) {
                        int countLike = (((PostModel) o).getLike() - 1);
                        txtLikeCount.setText(countLike > 1 ? countLike + " " + getString(R.string.likes) : countLike + " " + getString(R.string.like));
                        if (countLike <= 0) {
                            txtLikeCount.setVisibility(View.GONE);
                        }
                        postVM.disLike((PostModel) o);
                    } else {
                        int countLike = (((PostModel) o).getLike() + 1);
                        txtLikeCount.setText(countLike > 1 ? countLike + " " + getString(R.string.likes) : countLike + " " + getString(R.string.like));
                        txtLikeCount.setVisibility(View.VISIBLE);
                        postVM.addLike((PostModel) o);
                    }
                } else if (view.getId() == R.id.btnComment || view.getId() == R.id.llCountControl) {
                    showDialogComment((PostModel) o);
                } else if (view.getId() == R.id.postOption) {
                    showPostOption((PostModel) o);
                }
            }

            @Override
            public void onItemLongClick(View view, Object o) {

            }
        });
        postsAdapter.setHasStableIds(true);
        mBinding.rcPosts.setRefreshProgressStyle(ProgressStyle.Pacman);
        mBinding.rcPosts.setLoadingMoreProgressStyle(ProgressStyle.BallRotate);
        mBinding.rcPosts.setArrowImageView(R.drawable.mango);
        mBinding.rcPosts.setPullRefreshEnabled(true);
        RecyclerView.ItemAnimator animator = mBinding.rcPosts.getItemAnimator();
        if (animator instanceof SimpleItemAnimator) {
            ((SimpleItemAnimator) animator).setSupportsChangeAnimations(false);
        }
        mBinding.rcPosts.setLoadingListener(new XRecyclerView.LoadingListener() {
            @Override
            public void onRefresh() {
                postsAdapter.notifyDataSetChanged();
                postVM.onRefresh(userId);
            }

            @Override
            public void onLoadMore() {
                if (userId == 0) {
                    postVM.getMorePublicPost(NewsFeedApplication.getCurrentUserId());
                } else {
                    postVM.getMorePost(userId);
                }
            }
        });
        mBinding.rcPosts.setLayoutManager(new LinearLayoutManager(requireActivity(), RecyclerView.VERTICAL, false));
        mBinding.rcPosts.setAdapter(postsAdapter);

        mBinding.llAuthor.setOnClickListener(v -> {
            Intent intent = new Intent(requireActivity(), AddPostActivity.class);
            startActivityForResult(intent, REQUEST_ADD_POST);
        });

        mBinding.icSearch.setOnClickListener(v -> requireActivity().startActivity(new Intent(requireActivity(), SearchActivity.class)));
    }

    private void showPostOption(PostModel postModel) {
        DialogPostOption dialogPostOption = new DialogPostOption(id -> {
            if (id == R.id.btnDelPost) {
                showConfirmDeletePost(postModel);
            } else if (id == R.id.btnEditPost) {
                openEditPost(postModel);
            }
        });
        dialogPostOption.show(getChildFragmentManager(), dialogPostOption.getTag());
    }

    private void openEditPost(PostModel postModel) {
        Intent intent = new Intent(getActivity(), AddPostActivity.class);
        intent.putExtra("POST_MODEL", postModel);
        startActivityForResult(intent, REQUEST_EDIT_POST);
    }

    private void showConfirmDeletePost(PostModel postModel) {
        dialogConfirm = new DialogConfirm(getString(R.string.delete_post), getString(R.string.msg_delete_post), v -> {
            postVM.deletePost(postModel);
            dialogConfirm.dismiss();

            postVM.getListPost().remove(postModel.index - 1);
            postsAdapter.notifyItemRemoved(postModel.index - 1);
        });
        dialogConfirm.show(getChildFragmentManager(), dialogConfirm.getTag());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_ADD_POST) {
                mBinding.rcPosts.getDefaultRefreshHeaderView().setState(2);
                postVM.onRefresh(userId);
            }
            if (requestCode == REQUEST_EDIT_POST) {
                if (data != null) {
                    PostModel postModel = (PostModel) data.getSerializableExtra("POST_MODEL");
                    if (postModel != null) {
                        postVM.getListPost().set(postModel.index - 1, postModel);
                        postsAdapter.notifyItemChanged(postModel.index);
                    } else {
                        postsAdapter.notifyDataSetChanged();
                    }
                } else {
                    postsAdapter.notifyDataSetChanged();
                }
            }
        }
    }


    private void showDialogComment(PostModel post) {
        DialogComment dialogComment = new DialogComment(post, commentCount -> {
            itemIndex = post.index;
            postVM.getPostById(post);
        });
        dialogComment.show(getChildFragmentManager(), dialogComment.getTag());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            // Do whatever you want

            return true;
        }

        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    public void processFromVM(String action, View view, BaseViewModel viewModel, Throwable t) {
        super.processFromVM(action, view, viewModel, t);
        switch (action) {
            case "getPostsSuccess":
                requireActivity().runOnUiThread(() -> {
                    mBinding.rcPosts.refreshComplete();
                    mBinding.rcPosts.loadMoreComplete();
                    postsAdapter.notifyDataSetChanged();
                });
                break;
            case "getPostByIdSuccess":
                requireActivity().runOnUiThread(() -> {
                    postsAdapter.notifyItemChanged(itemIndex);
                });
                break;

            case "getPostsFail":
                requireActivity().runOnUiThread(() -> {
                    Toast.makeText(requireActivity(), R.string.some_thing_wrong, Toast.LENGTH_LONG).show();
                });
                mBinding.rcPosts.refreshComplete();
                break;

            case "noMore":
                requireActivity().runOnUiThread(() -> {
                    mBinding.rcPosts.refreshComplete();
                    mBinding.rcPosts.loadMoreComplete();
                    mBinding.rcPosts.setNoMore(true);
                });

                break;
        }
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_post;
    }


    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return PostVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }
}
