package com.nextsolutions.newfeed.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.fragment.app.FragmentTransaction;

import com.bumptech.glide.Glide;
import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.entity.LocalMedia;
import com.nextsolutions.newfeed.R;
import com.nextsolutions.newfeed.application.NewsFeedApplication;
import com.nextsolutions.newfeed.databinding.ProfileMainBinding;
import com.nextsolutions.newfeed.util.IntentConstants;
import com.nextsolutions.newfeed.util.StringUtils;
import com.nextsolutions.newfeed.util.TsUtils;
import com.nextsolutions.newfeed.view.dialog.DialogChangeAvatar;
import com.nextsolutions.newfeed.view.fragment.PostsFragment;
import com.nextsolutions.newfeed.viewmodel.PostVM;
import com.nextsolutions.newfeed.widget.glide.GlideEngine;
import com.tsolution.base.BaseActivity;
import com.tsolution.base.BaseViewModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ProfileActivity extends BaseActivity<ProfileMainBinding> {
    private PostVM postVM;
    private int userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        postVM = (PostVM) viewModel;
        if (getIntent() != null && getIntent().hasExtra(IntentConstants.INTENT_USER_ID)) {
            userId = getIntent().getIntExtra(IntentConstants.INTENT_USER_ID, -1);
        }
        postVM.isProfile.set(userId == NewsFeedApplication.getCurrentUser().getId());
        if (userId != NewsFeedApplication.getCurrentUser().getId()) {
            postVM.callApiCheckFollow(NewsFeedApplication.getCurrentUserId(), userId);
        }

        postVM.getProfileUser(userId);
        postVM.getFollows(userId, 0);
        postVM.getFollowsMe(userId, 0);
        //
        initView();


    }

    private void initView() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        PostsFragment postsFragment = new PostsFragment(userId);
        ft.replace(R.id.frame_post, postsFragment);
        ft.commit();
        binding.imgBack.setOnClickListener(v -> finish());
        binding.imgAvatar.setOnClickListener(v -> {
            if (TsUtils.isObjectNotNull(postVM.getUser().get())){
                previewImage(postVM.getUser().get().avatar());
            }
        });

        binding.btnFollow.setOnClickListener(v -> {
            if (postVM.checkFollow.get()) {
                postVM.unfollow(NewsFeedApplication.getCurrentUserId(), userId);
                postVM.checkFollow.set(false);
            } else {
                postVM.follow(NewsFeedApplication.getCurrentUserId(), userId);
                postVM.checkFollow.set(true);
            }
        });
        binding.txtFollowings.setOnClickListener(v -> {
            openFollow(0);
        });
        binding.lbFollowings.setOnClickListener(v -> {
            openFollow(0);
        });

        binding.txtFollowers.setOnClickListener(v -> {
            openFollow(1);
        });
        binding.lbFollowers.setOnClickListener(v -> {
            openFollow(1);
        });
    }
    private void previewImage(String url) {
        List<LocalMedia> lstMedia = new ArrayList<>();
        LocalMedia localMedia = new LocalMedia();
        localMedia.setPath(url);

        lstMedia.add(localMedia);

        PictureSelector.create(this)
                .themeStyle(R.style.picture_default_style)
                .isNotPreviewDownload(true)
                .loadImageEngine(GlideEngine.createGlideEngine())
                .openExternalPreview(0, lstMedia);
    }

    private void openFollow(int position) {
        Intent intent = new Intent(this, ActivityFollow.class);
        intent.putExtra("position", position);
        intent.putExtra(IntentConstants.INTENT_USER_ID, userId);
        startActivity(intent);
    }

    @Override
    public void processFromVM(String action, View view, BaseViewModel viewModel, Throwable t) {
        super.processFromVM(action, view, viewModel, t);
    }


    @Override
    public int getLayoutRes() {
        return R.layout.profile_main;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return PostVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }
}
