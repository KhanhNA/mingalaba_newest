package com.nextsolutions.newfeed.view.dialog;

import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.nextsolutions.newfeed.R;
import com.nextsolutions.newfeed.databinding.DialogPostOptionBinding;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

public class DialogPostOption extends BottomSheetDialogFragment {
    DialogPostOptionBinding binding;
    OnDismiss onDismiss;


    public DialogPostOption(OnDismiss onDismiss){
        this.onDismiss = onDismiss;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_post_option, container, false);

        binding.btnDismiss.setOnClickListener(v -> this.dismiss());
        DisplayMetrics displayMetrics = new DisplayMetrics();
        requireActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        binding.root.setMinHeight(height / 2);

        binding.btnDelPost.setOnClickListener(v -> {
            onDismiss.onDismiss(R.id.btnDelPost);
            this.dismiss();
        });
        binding.btnEditPost.setOnClickListener(v -> {
            onDismiss.onDismiss(R.id.btnEditPost);
            this.dismiss();
        });
        binding.btnRange.setOnClickListener(v -> {
            onDismiss.onDismiss(R.id.btnRange);
            this.dismiss();
        });

        return binding.getRoot();
    }

    public interface OnDismiss {
        void onDismiss(Integer id);
    }
}
