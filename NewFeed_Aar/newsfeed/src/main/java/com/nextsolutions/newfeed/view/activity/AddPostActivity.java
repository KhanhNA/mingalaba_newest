package com.nextsolutions.newfeed.view.activity;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.StrictMode;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.bumptech.glide.Glide;
import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.animators.AnimationType;
import com.luck.picture.lib.config.PictureConfig;
import com.luck.picture.lib.config.PictureMimeType;
import com.luck.picture.lib.decoration.GridSpacingItemDecoration;
import com.luck.picture.lib.entity.LocalMedia;
import com.luck.picture.lib.language.LanguageConfig;
import com.luck.picture.lib.listener.OnResultCallbackListener;
import com.luck.picture.lib.tools.ScreenUtils;
import com.nextsolutions.newfeed.R;
import com.nextsolutions.newfeed.adapter.FontsAdapter;
import com.nextsolutions.newfeed.adapter.GridImageAdapter;
import com.nextsolutions.newfeed.application.NewsFeedApplication;
import com.nextsolutions.newfeed.databinding.ActivityAddPostBinding;
import com.nextsolutions.newfeed.model.PostModel;
import com.nextsolutions.newfeed.model.dto.OptionDTO;
import com.nextsolutions.newfeed.util.StringUtils;
import com.nextsolutions.newfeed.util.TsUtils;
import com.nextsolutions.newfeed.view.dialog.ListDialogFragment;
import com.nextsolutions.newfeed.viewmodel.AddPostVM;
import com.nextsolutions.newfeed.widget.FullyGridLayoutManager;
import com.nextsolutions.newfeed.widget.glide.GlideEngine;
import com.tsolution.base.BaseActivity;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.listener.AdapterListener;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;


public class AddPostActivity extends BaseActivity<ActivityAddPostBinding> {
    private AddPostVM addPostVM;
    private static String TAG = "AddPostVM";
    String titleColor = "#24282C";
    FontsAdapter fontsAdapter;
    private List<String> fonts;
    private ListDialogFragment dialogFragment;
    private GridImageAdapter imageSelectAdapter;
    private boolean isEdit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            hideKeyBoard();
            addPostVM = (AddPostVM) viewModel;



            initData();
            initView();
            initAdapter(savedInstanceState);

            PostModel postModel = (PostModel) getIntent().getSerializableExtra("POST_MODEL");
            if(postModel != null){
                isEdit = true;
                binding.txtPost.setText(R.string.update);
                addPostVM.getPostObs().set(postModel);
                if(TsUtils.isNotNull(postModel.getUrl())){
                    List<LocalMedia> localMediaList = new ArrayList<>();
                    for(String url : postModel.getUrl()){
                        LocalMedia localMedia = new LocalMedia();
                        localMedia.setPath(url);
                        localMediaList.add(localMedia);
                    }
                    addPostVM.setDataFileChoose(localMediaList);
                    imageSelectAdapter.setList(localMediaList);
                }
                setMessage(postModel.isIsprivate());
            }else {
                setMessage(false);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setMessage(boolean isPrivate) {
        addPostVM.setMessageFriendAllView(getString(R.string.friend_all_view));
        addPostVM.setMessageFriendAll(getString(R.string.friend_all));
        addPostVM.setMessageOnlyMe(getString(R.string.only_me));
        addPostVM.setMessageOnlyMeView(getString(R.string.only_me_view));
        addPostVM.getLstPermissionView(isPrivate);
    }

    private void initData() {
        initFont();
    }

    private void initFont() {
        try {
            AssetManager assetManager = getAssets();
            fonts = Arrays.asList(Objects.requireNonNull(assetManager.list("fonts")));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void initView() {

        Glide.with(this).load(NewsFeedApplication.getCurrentUser().getAvatar())
                .error(R.drawable.ic_stub)
                .into(binding.imgAvatar);
        binding.txtFullName.setText(NewsFeedApplication.getCurrentUser().getDisplay_name());

        binding.imgBack.setOnClickListener(v -> finish());
        binding.pickerImage.setOnClickListener(v -> {
            openGallery();
        });
        binding.pickerImage2.setOnClickListener(v -> {
            openGallery();
        });
        binding.pickerColor.setOnClickListener(v -> {
            binding.cpvFontTextColor.setVisibility(binding.cpvFontTextColor.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
        });
        fontsAdapter = new FontsAdapter(this, fonts, (view, position) -> {
            Typeface type = Typeface.createFromAsset(getAssets(), "fonts/" + fonts.get(position));
            binding.txtPostTitle.setTypeface(type);
            fontsAdapter.notifyDataSetChanged();
            addPostVM.getPostObs().get().setString_font(fonts.get(position));
        });
        binding.rcFonts.setAdapter(fontsAdapter);
        binding.rcFonts.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

        binding.txtPostTitle.requestFocus();
        binding.cpvFontTextColor.setOnColorChangeListener(color -> {
            binding.txtPostTitle.setTextColor(Color.parseColor(color));
            titleColor = color;
            addPostVM.getPostObs().get().setString_color(color);
            fontsAdapter.selectedColor = color;
            if (fontsAdapter.selected != -1) {
                fontsAdapter.notifyItemChanged(fontsAdapter.selected);
            }
        });

        binding.txtPost.setOnClickListener(v -> {
            String txtPostTitle = binding.txtPostTitle.getText().toString();
            if (!StringUtils.isNullOrEmpty(txtPostTitle)) {
                if (addPostVM.getIsNotEmpty().get()) {
                    addPostVM.uploadPostWithFile(isEdit);
                } else {
                    addPostVM.uploadPost(isEdit,null);
                }
            } else {
                Toast.makeText(AddPostActivity.this, R.string.title_post_not_empty, Toast.LENGTH_LONG).show();
            }

        });

        binding.layoutPermission.setOnClickListener(v ->
        {
            dialogFragment = new ListDialogFragment(R.layout.item_permission_view,
                    R.string.list_of_permission_view,
                    addPostVM.getLstPermissionView(addPostVM.getPostObs().get().isIsprivate()), new AdapterListener() {
                @Override
                public void onItemClick(View view, Object o) {
                    addPostVM.getObOptionDTO().set((OptionDTO) o);
                    addPostVM.getPostObs().get().setIsprivate(((OptionDTO) o).getIsOnlyMe());
                    dialogFragment.dismiss();
                }

                @Override
                public void onItemLongClick(View view, Object o) {

                }
            });
            dialogFragment.show(getSupportFragmentManager(), "fragment_permission");

        });
    }


    @Override
    public void processFromVM(String action, View view, BaseViewModel viewModel, Throwable t) {
        super.processFromVM(action, view, viewModel, t);
        switch (action) {

            case "uploadPostSuccess":
                runOnUiThread(() -> {
                    hideKeyBoard();
                    Toast.makeText(AddPostActivity.this, R.string.uploadPostSuccess, Toast.LENGTH_LONG).show();
                    Intent intent = null;
                    if(isEdit){
                        intent = getIntent();
                        intent.putExtra("POST_MODEL", addPostVM.getPostObs().get());
                    }
                    setResult(RESULT_OK, intent);

                    finish();
                });

                break;
            case "uploadPostFail":
                runOnUiThread(() -> {
                    hideKeyBoard();
                    Toast.makeText(AddPostActivity.this, R.string.uploadPostFail, Toast.LENGTH_LONG).show();
                });
                break;
        }
    }

    private void initListImageSelected() {
        FullyGridLayoutManager manager = new FullyGridLayoutManager(this,
                4, GridLayoutManager.VERTICAL, false);
        binding.rcImages.setLayoutManager(manager);

        binding.rcImages.addItemDecoration(new GridSpacingItemDecoration(4,
                ScreenUtils.dip2px(this, 8), false));
    }

    private void initAdapter(Bundle savedInstanceState) {
        initListImageSelected();
        imageSelectAdapter = new GridImageAdapter(AddPostActivity.this, onAddPicClickListener);
        if (savedInstanceState != null && savedInstanceState.getParcelableArrayList("selectorList") != null) {
            imageSelectAdapter.setList(savedInstanceState.getParcelableArrayList("selectorList"));
        }
        imageSelectAdapter.setSelectMax(6);
        imageSelectAdapter.setOnClear(() -> addPostVM.getIsNotEmpty().set(false));
        binding.rcImages.setAdapter(imageSelectAdapter);
        imageSelectAdapter.setOnItemClickListener((v, position) -> {
            if(v.getId() == R.id.iv_del){
                Toast.makeText(this,position + "", Toast.LENGTH_SHORT).show();
            }else {
                openOptionImage(position);
            }
        });
        imageSelectAdapter.setItemLongClickListener((holder, position, v) -> {
        });
    }

    private void openOptionImage(int position) {
        List<LocalMedia> selectList = imageSelectAdapter.getData();
        if (selectList.size() > 0) {
            LocalMedia media = selectList.get(position);
            String mimeType = media.getMimeType();
            int mediaType = PictureMimeType.getMimeType(mimeType);
            switch (mediaType) {
                case PictureConfig.TYPE_VIDEO:
                    PictureSelector.create(AddPostActivity.this)
                            .themeStyle(R.style.picture_default_style)
                            .externalPictureVideo(TextUtils.isEmpty(media.getAndroidQToPath()) ? media.getPath() : media.getAndroidQToPath());
                    break;
                default:
                    PictureSelector.create(AddPostActivity.this)
                            .themeStyle(R.style.picture_default_style)
                            .setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED)
                            .loadImageEngine(GlideEngine.createGlideEngine())
                            .openExternalPreview(position, selectList);
                    break;
            }
        }
    }

    private GridImageAdapter.onAddPicClickListener onAddPicClickListener = () -> openGallery();

    private void openGallery() {
        PictureSelector.create(AddPostActivity.this)
                .openGallery(1)
                .loadImageEngine(GlideEngine.createGlideEngine())
                .theme(R.style.picture_WeChat_style)
                .isWeChatStyle(true)
                .setLanguage(LanguageConfig.ENGLISH)
                .isPageStrategy(true)
                .setRecyclerAnimationMode(AnimationType.DEFAULT_ANIMATION)
                .isWithVideoImage(true)
                .isMaxSelectEnabledMask(true)
                .maxSelectNum(6)
                .minSelectNum(1)
                .maxVideoSelectNum(1)
                .imageSpanCount(4)
                .setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED)
                .isOriginalImageControl(false)
                .selectionMode(PictureConfig.MULTIPLE)
                .isSingleDirectReturn(false)
                .previewImage(true)
                .previewVideo(false)
                .isCamera(true)
                .isZoomAnim(true)
                .compress(true)
                .compressQuality(80)
                .synOrAsy(true)
                .hideBottomControls(false)//
                .freeStyleCropEnabled(true)
                .circleDimmedLayer(true)
                .showCropFrame(true)
                .showCropGrid(true)
                .openClickSound(false)
                .selectionMedia(imageSelectAdapter.getData())
                .cutOutQuality(90)
                .minimumCompressSize(100)
                .forResult(new MyResultCallback(imageSelectAdapter, addPostVM));
    }

    private static class MyResultCallback implements OnResultCallbackListener<LocalMedia> {
        private WeakReference<GridImageAdapter> mAdapterWeakReference;
        private AddPostVM addPostVM;

        public MyResultCallback(GridImageAdapter adapter, AddPostVM addPostVM) {
            super();
            this.mAdapterWeakReference = new WeakReference<>(adapter);
            this.addPostVM = addPostVM;
        }

        @Override
        public void onResult(List<LocalMedia> result) {
            addPostVM.setDataFileChoose(result);
            for (LocalMedia media : result) {
                Log.d(TAG, "PATH ORIGIN: " + media.getOriginalPath());
                Log.d(TAG, "PATH: " + media.getCompressPath());
            }
            if (mAdapterWeakReference.get() != null) {
                mAdapterWeakReference.get().setList(result);
                mAdapterWeakReference.get().notifyDataSetChanged();
            }
        }

        @Override
        public void onCancel() {
            Log.i(TAG, "PictureSelector Cancel");
        }
    }

    @Override
    public int getLayoutRes() {
        return R.layout.activity_add_post;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return AddPostVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }


}
