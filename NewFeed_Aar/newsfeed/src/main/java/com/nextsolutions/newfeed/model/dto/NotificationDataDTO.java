package com.nextsolutions.newfeed.model.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NotificationDataDTO {
    private String click_action;
    private Long itemId;
    private String message;
    private String title;
}
