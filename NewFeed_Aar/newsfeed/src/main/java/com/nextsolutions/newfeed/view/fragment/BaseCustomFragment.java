package com.nextsolutions.newfeed.view.fragment;


import android.app.Application;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.CallSuper;
import androidx.annotation.IdRes;
import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.nextsolutions.newfeed.listener.DefaultFunctionActivity;
import com.tsolution.base.BR;
import com.tsolution.base.BaseViewModel;


public abstract class BaseCustomFragment<V extends ViewDataBinding> extends Fragment implements DefaultFunctionActivity {
    protected RecyclerView recyclerView;
    protected BaseViewModel viewModel;
    protected V binding;

    @Nullable
    @CallSuper
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        try {
            super.onCreateView(inflater, container, savedInstanceState);
            binding = DataBindingUtil.inflate(inflater, getLayoutRes(), container, false);
            if (getVMClass() != null) {
                init(savedInstanceState, getLayoutRes(), getVMClass(), getRecycleResId());
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return binding.getRoot();
    }

    public void init(@Nullable Bundle savedInstanceState, @LayoutRes int layoutId,
                     Class<? extends BaseViewModel> clazz, @IdRes int recyclerViewId) throws Throwable {
        viewModel = clazz.getDeclaredConstructor(Application.class).newInstance(requireActivity().getApplication());//ViewModelProviders.of(getActivity()).get(clazz);
        View view = binding.getRoot();
        binding.setVariable(BR.viewModel, viewModel);
        viewModel.setView(this::processFromVM);
        if (recyclerViewId != 0) {
            recyclerView = view.findViewById(recyclerViewId);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
            recyclerView.setLayoutManager(layoutManager);
        }
    }

    public void init(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState, @LayoutRes int layoutId,
                     Class<? extends BaseViewModel> clazz, @IdRes int recyclerViewId) throws Exception {
        viewModel = clazz.getDeclaredConstructor(Application.class).newInstance(requireActivity().getApplication());//ViewModelProviders.of(getActivity()).get(clazz);
        View view = binding.getRoot();
        binding.setVariable(BR.viewModel, viewModel);
        binding.setVariable(BR.listener, this);
        viewModel.setView(this::processFromVM);
        if (recyclerViewId != 0) {
            recyclerView = view.findViewById(recyclerViewId);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
            recyclerView.setLayoutManager(layoutManager);
        }
    }


    @Override
    public void processFromVM(String action, View view, BaseViewModel viewModel, Throwable t) {

    }

    public BaseViewModel getViewModel() {
        return viewModel;
    }

    public void setViewModel(BaseViewModel viewModel) {
        this.viewModel = viewModel;
    }

}
