package com.nextsolutions.newfeed.view.viewHolder;

import com.nextsolutions.newfeed.BR;
import com.tsolution.base.listener.AdapterListener;

import androidx.annotation.NonNull;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;

public class VideoViewHolder extends RecyclerView.ViewHolder {
    private ViewDataBinding itemProductBinding;

    public VideoViewHolder(@NonNull ViewDataBinding itemView) {
        super(itemView.getRoot());
        this.itemProductBinding = itemView;
    }

    public void bind(Object obj, AdapterListener listener) {
        itemProductBinding.setVariable(BR.viewHolder, obj);
        itemProductBinding.setVariable(BR.listenerAdapter, listener);
        itemProductBinding.executePendingBindings();
    }
}
