package com.nextsolutions.newfeed.adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.nextsolutions.newfeed.view.fragment.FollowDetailFragment;

public class FollowPagerAdapter extends FragmentStatePagerAdapter {
    String[] title;
    int userId;

    public FollowPagerAdapter(@NonNull FragmentManager fm, int userId, String[] title) {
        super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        this.title = title;
        this.userId = userId;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new FollowDetailFragment(true, userId);
            case 1:
                return new FollowDetailFragment(false, userId);
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return title[position];
    }
}
