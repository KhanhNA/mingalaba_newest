package com.nextsolutions.newfeed.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.nextsolutions.newfeed.R;
import com.nextsolutions.newfeed.application.NewsFeedApplication;
import com.nextsolutions.newfeed.listener.LoginCallback;
import com.nextsolutions.newfeed.model.PeopleModel;
import com.nextsolutions.newfeed.util.IntentConstants;
import com.nextsolutions.newfeed.util.StringUtils;
import com.nextsolutions.newfeed.view.fragment.PostsFragment;
import com.nextsolutions.newfeed.view.fragment.SearchActivity;
import com.tsolution.base.CommonActivity;

import androidx.appcompat.app.AppCompatActivity;

import okhttp3.OkHttpClient;
import okhttp3.Request;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        findViewById(R.id.btnPosts).setOnClickListener(v -> {
            Intent intent = new Intent(this, CommonActivity.class);
            intent.putExtra("FRAGMENT", PostsFragment.class);
            startActivity(intent);
        });

        findViewById(R.id.btnAddPost).setOnClickListener(v -> startActivity(new Intent(MainActivity.this, AddPostActivity.class)));

        findViewById(R.id.btnProfile).setOnClickListener(v -> {
            Intent intent = new Intent(this, ProfileActivity.class);
            intent.putExtra(IntentConstants.INTENT_USER_ID, NewsFeedApplication.getCurrentUserId());
            startActivity(intent);
        });

        findViewById(R.id.btnPostDetail).setOnClickListener(v -> {
            Intent intent = new Intent(this, PostDetailActivity.class);
            intent.putExtra(IntentConstants.INTENT_ITEM_ID, "845");
            startActivity(intent);
        });

        findViewById(R.id.btnSearch).setOnClickListener(v -> startActivity(new Intent(MainActivity.this, SearchActivity.class)));

//        getProfileUser();
        NewsFeedApplication.getInstance().getProfileUser("@0975086498:demo.nextsolutions.com.vn",
                "MDAyN2xvY2F0aW9uIGRlbW8ubmV4dHNvbHV0aW9ucy5jb20udm4KMDAxM2lkZW50aWZpZXIga2V5CjAwMTBjaWQgZ2VuID0gMQowMDM4Y2lkIHVzZXJfaWQgPSBAMDk3NTA4NjQ5ODpkZW1vLm5leHRzb2x1dGlvbnMuY29tLnZuCjAwMTZjaWQgdHlwZSA9IGFjY2VzcwowMDIxY2lkIG5vbmNlID0gK1VaTHpfa0o7ZXFPbUI7VwowMDJmc2lnbmF0dXJlIDkBkzaFWq9Ie60HUV-7VK3MX2fY_YCjzOpUdqEDwEpqCg",
                "mingalaba", new LoginCallback() {
            @Override
            public void loginSuccess(PeopleModel userData) {
                Log.e("Login", userData.getId() + "");
            }

            @Override
            public void loginError(String message) {
                Log.e("Login", message);

            }
        });
        if (getIntent() != null) {
            getIntentData();
        }
    }

    private void getIntentData() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String actionOpen = extras.getString(IntentConstants.INTENT_CLICK_ACTION, "");
            String itemId = extras.getString(IntentConstants.INTENT_ITEM_ID, "");
            if (StringUtils.isNotNullAndNotEmpty(actionOpen)) {
                Intent intent1 = new Intent(actionOpen);
                intent1.putExtra(IntentConstants.INTENT_ITEM_ID, itemId);
                startActivity(intent1);
            }
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        getIntentData();
    }


}
