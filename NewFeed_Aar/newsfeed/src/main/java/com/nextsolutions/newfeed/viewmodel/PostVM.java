package com.nextsolutions.newfeed.viewmodel;

import android.app.Application;
import android.util.Log;

import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;
import com.apollographql.apollo.exception.ApolloHttpException;
import com.apollographql.apollo.exception.ApolloNetworkException;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.nextsolutions.AddLikeMutation;
import com.nextsolutions.CheckFollowQuery;
import com.nextsolutions.DeletePostMutation;
import com.nextsolutions.DisFollowMutation;
import com.nextsolutions.DisLikeMutation;
import com.nextsolutions.FollowMutation;
import com.nextsolutions.GetFollowsMeQuery;
import com.nextsolutions.GetFollowsQuery;
import com.nextsolutions.GetPublicItemQuery;
import com.nextsolutions.ItemQuery;
import com.nextsolutions.OneItemQuery;
import com.nextsolutions.UserQuery;
import com.nextsolutions.newfeed.application.NewsFeedApplication;
import com.nextsolutions.newfeed.model.PeopleModel;
import com.nextsolutions.newfeed.model.PostModel;
import com.nextsolutions.newfeed.util.GsonUTCDateAdapter;
import com.nextsolutions.newfeed.util.IntentConstants;
import com.nextsolutions.newfeed.util.TsUtils;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.exceptionHandle.AppException;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PostVM extends BaseViewModel {
    private ObservableBoolean isLoading = new ObservableBoolean();
    public ObservableField<UserQuery.User> user = new ObservableField<>();
    public ObservableField<Integer> totalFollow;
    public ObservableField<Integer> totalFollowMe;
    public List<PeopleModel> followsData;
    public ObservableBoolean checkFollow = new ObservableBoolean();
    public ObservableBoolean isProfile = new ObservableBoolean(false);
    private List<PostModel> listPost;

    //vị trí item vừa thao tác
    private int actionPosition;

    private int currentPage = 0;
    private boolean isLoadFull;

    public PostVM(@NonNull Application application) {
        super(application);
        listPost = new ArrayList<>();
        followsData = new ArrayList<>();
        totalFollow = new ObservableField<>();
        totalFollow.set(0);
        totalFollowMe = new ObservableField<>();
        totalFollowMe.set(0);
    }

    public void getProfileUser(int idUser) {
        NewsFeedApplication.getInstance().getApi().query(UserQuery.builder().id(idUser).build()).enqueue(new ApolloCall.Callback<UserQuery.Data>() {
            @Override
            public void onResponse(@NotNull Response<UserQuery.Data> response) {
                user.set(response.data().user());
                try {
                    view.action(IntentConstants.GET_PROFILE_SUCCESS, null, null, null);
                } catch (AppException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {

            }

        });
    }

    public void deletePost(PostModel postModel) {
        NewsFeedApplication.getInstance().getApi().mutate(DeletePostMutation.builder().itemId(postModel.getId()).build()).enqueue(new ApolloCall.Callback<DeletePostMutation.Data>() {
            @Override
            public void onResponse(@NotNull Response<DeletePostMutation.Data> response) {
//                try {
//                    view.action(IntentConstants.DELETE_SUCCESS, null, null, null);
//                } catch (AppException e) {
//                    e.printStackTrace();
//                }
                Log.e("DELETE POST", "success");
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {
//                try {
//                    view.action(IntentConstants.UNFOLLOW_FAIL, null, null, null);
//                } catch (AppException ex) {
//                    ex.printStackTrace();
//                }
                Log.e("DELETE POST", "failure");

            }
        });
    }

    public void getFollows(int userId, int page) {
        NewsFeedApplication.getInstance().getApi().query(GetFollowsQuery.builder().userId(userId).page_number(page).build()).enqueue(new ApolloCall.Callback<GetFollowsQuery.Data>() {
            @Override
            public void onResponse(@NotNull Response<GetFollowsQuery.Data> response) {
                if (response.data() != null && TsUtils.isNotNull(response.data().getFollows().data())) {
                    totalFollow.set(response.data().getFollows().total());
                    Gson gson = new Gson();
                    for (GetFollowsQuery.Data1 item : response.data().getFollows().data()) {
                        String json = gson.toJson(item);
                        PeopleModel peopleModel = gson.
                                fromJson(json, PeopleModel.class);
                        followsData.add(peopleModel);
                    }
                    try {
                        view.action("getFollowSuccess", null, null, null);
                    } catch (AppException e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        view.action("getFollowError", null, null, null);
                    } catch (AppException e) {
                        e.printStackTrace();
                    }
                    isLoadFull = true;
                }
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {
                try {
                    view.action("getFollowError", null, null, null);
                } catch (AppException ex) {
                    ex.printStackTrace();
                }
                isLoadFull = true;
            }
        });
    }

    public void getFollowsMe(int userId, int page) {
        NewsFeedApplication.getInstance().getApi().query(GetFollowsMeQuery.builder().userId(userId).page_number(page).build()).enqueue(new ApolloCall.Callback<GetFollowsMeQuery.Data>() {
            @Override
            public void onResponse(@NotNull Response<GetFollowsMeQuery.Data> response) {
                if (response.data() != null && TsUtils.isNotNull(response.data().getFollowsMe().data())) {
                    totalFollowMe.set(response.data().getFollowsMe().total());
                    Gson gson = new Gson();
                    for (GetFollowsMeQuery.Data1 item : response.data().getFollowsMe().data()) {
                        String json = gson.toJson(item);
                        PeopleModel peopleModel = gson.
                                fromJson(json, PeopleModel.class);
                        followsData.add(peopleModel);
                    }
                    try {
                        view.action("getFollowSuccess", null, null, null);
                    } catch (AppException e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        view.action("getFollowError", null, null, null);
                    } catch (AppException e) {
                        e.printStackTrace();
                    }
                    isLoadFull = true;
                }
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {
                try {
                    view.action("getFollowError", null, null, null);
                } catch (AppException ex) {
                    ex.printStackTrace();
                }
                isLoadFull = true;
            }
        });
    }

    public void follow(int userId, int targetUserId) {
        NewsFeedApplication.getInstance().getApi().mutate(FollowMutation.builder().userId(userId).target_userId(targetUserId).build()).enqueue(new ApolloCall.Callback<FollowMutation.Data>() {
            @Override
            public void onResponse(@NotNull Response<FollowMutation.Data> response) {
                try {
                    view.action(IntentConstants.FOLLOW_SUCCESS, null, null, null);
                } catch (AppException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {
                try {
                    view.action(IntentConstants.FOLLOW_FAIL, null, null, null);
                } catch (AppException a) {
                    a.printStackTrace();
                }
            }
        });
    }

    public void unfollow(int userId, int targetId) {
        NewsFeedApplication.getInstance().getApi().mutate(DisFollowMutation.builder().userId(userId).target_userId(targetId).build()).enqueue(new ApolloCall.Callback<DisFollowMutation.Data>() {
            @Override
            public void onResponse(@NotNull Response<DisFollowMutation.Data> response) {
                try {
                    view.action(IntentConstants.UNFOLLOW_SUCCESS, null, null, null);
                } catch (AppException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {
                try {
                    view.action(IntentConstants.UNFOLLOW_FAIL, null, null, null);
                } catch (AppException ex) {
                    ex.printStackTrace();
                }
            }
        });
    }

    public void callApiCheckFollow(int userId, int targetId) {
        NewsFeedApplication.getInstance().getApi().query(CheckFollowQuery.builder().userId(userId).target_userId(targetId).build()).enqueue(new ApolloCall.Callback<CheckFollowQuery.Data>() {
            @Override
            public void onResponse(@NotNull Response<CheckFollowQuery.Data> response) {
                checkFollow.set(response.data().checkFollow());
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {

            }
        });
    }

    public void getPostById(PostModel postModel) {
        NewsFeedApplication.getInstance().getApi().query(OneItemQuery.builder()
                .userId(postModel.getUser_id())
                .itemId(postModel.getId()).build()
        ).enqueue(new ApolloCall.Callback<OneItemQuery.Data>() {
            @Override
            public void onResponse(@NotNull Response<OneItemQuery.Data> response) {
                if (response.data() != null && response.data().oneItem() != null) {
                    GsonBuilder jsonBuilder = new GsonBuilder();
                    jsonBuilder.registerTypeAdapter(Date.class, new GsonUTCDateAdapter());
                    Gson gson = jsonBuilder.create();
                    String json = gson.toJson(response.data().oneItem());
                    PostModel post = gson.
                            fromJson(json, PostModel.class);
                    post.setIslike(postModel.getIslike());
                    listPost.set(postModel.index - 1, post);
                    try {
                        view.action("getPostByIdSuccess", null, null, null);
                    } catch (AppException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {
            }
        });
    }


    public void getPosts(int userId, int page) {
        NewsFeedApplication.getInstance().getApi().query(ItemQuery.builder().userId(userId).page_number(page).build())
                .enqueue(new ApolloCall.Callback<ItemQuery.Data>() {
                    @Override
                    public void onResponse(@NotNull Response<ItemQuery.Data> response) {
                        try {
                            if (response.data().items() != null && response.data().items().size() > 0) {
                                listPost.addAll(convertData(response.data().items()));
                                view.action("getPostsSuccess", null, null, null);
                            } else {
                                isLoadFull = true;
                                view.action("noMore", null, null, null);
                            }
                        } catch (AppException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(@NotNull ApolloException e) {
                        try {
                            isLoading.set(false);
                            view.action("getPostsFail", null, null, null);
                        } catch (AppException ex) {
                            ex.printStackTrace();
                        }
                    }
                });
    }

    public void getPostPublic(int userId, int page) {
        NewsFeedApplication.getInstance().getApi().query(GetPublicItemQuery.builder()
                .userId(userId).page_number(page).build())
                .enqueue(new ApolloCall.Callback<GetPublicItemQuery.Data>() {
                             @Override
                             public void onResponse(@NotNull Response<GetPublicItemQuery.Data> response) {
                                 try {
                                     if (response.data().publicItems() != null && response.data().publicItems().size() > 0) {
                                         listPost.addAll(convertData2(response.data().publicItems()));
                                         view.action("getPostsSuccess", null, null, null);
                                     } else {
                                         isLoadFull = true;
                                         view.action("noMore", null, null, null);
                                     }
                                 } catch (AppException e) {
                                     e.printStackTrace();
                                 }
                             }

                    @Override
                    public void onNetworkError(@NotNull ApolloNetworkException e) {
                        super.onNetworkError(e);
                        Log.d("xxxe", "onNetworkError: ");

                    }

                    @Override
                    public void onHttpError(@NotNull ApolloHttpException e) {
                        super.onHttpError(e);
                        Log.d("xxx", "onHttpError: ");
                    }

                    @Override
                             public void onFailure(@NotNull ApolloException e) {
                                 try {
                                     isLoading.set(false);
                                     view.action("getPostsFail", null, null, null);
                                 } catch (AppException ex) {
                                     ex.printStackTrace();
                                 }
                             }
                         }
                );
    }

    public void getMoreFollowMe(int userId) {
        if (!isLoadFull) {
            currentPage++;
            getFollowsMe(userId, currentPage);
        }
    }

    public void getMoreFollowing(int userId) {
        if (!isLoadFull) {
            currentPage++;
            getFollows(userId, currentPage);
        }
    }

    public void getMorePost(int userId) {
        if (!isLoadFull) {
            currentPage++;
            getPosts(userId, currentPage);
        }
    }

    public void getMorePublicPost(int userId) {
        if (!isLoadFull) {
            currentPage++;
            getPostPublic(userId, currentPage);
        }
    }

    private List<PostModel> convertData(List<ItemQuery.Item> items) {
        if (items != null) {
            GsonBuilder jsonBuilder = new GsonBuilder();
            jsonBuilder.registerTypeAdapter(Date.class, new GsonUTCDateAdapter());
            Gson gson = jsonBuilder.create();
            List<PostModel> result = new ArrayList<>();
            for (ItemQuery.Item item : items) {
                String json = gson.toJson(item);
                PostModel post = gson.
                        fromJson(json, PostModel.class);
                result.add(post);
            }
            return result;
        }
        return null;
    }

    private List<PostModel> convertData2(List<GetPublicItemQuery.PublicItem> items) {
        if (items != null) {
            GsonBuilder jsonBuilder = new GsonBuilder();
            jsonBuilder.registerTypeAdapter(Date.class, new GsonUTCDateAdapter());
            Gson gson = jsonBuilder.create();
            List<PostModel> result = new ArrayList<>();
            for (GetPublicItemQuery.PublicItem item : items) {
                String json = gson.toJson(item);
                PostModel post = gson.
                        fromJson(json, PostModel.class);
                result.add(post);
            }
            return result;
        }
        return null;
    }

    public void addLike(PostModel item) {
        NewsFeedApplication.getInstance().getApi().mutate(AddLikeMutation.builder().user_id(NewsFeedApplication.getCurrentUserId())
                .target_item_id(item.getId()).build()).enqueue(new ApolloCall.Callback<AddLikeMutation.Data>() {
            @Override
            public void onResponse(@NotNull Response<AddLikeMutation.Data> response) {
                PostModel result = listPost.get(item.index - 1);
                result.setIslike(true);
                result.setLike(item.getLike() + 1);
                actionPosition = item.index - 1;
                try {
                    view.action("addLikeSuccess", null, null, null);
                } catch (AppException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {

            }
        });
    }

    public void disLike(PostModel item) {
        NewsFeedApplication.getInstance().getApi().mutate(DisLikeMutation.builder().user_id(NewsFeedApplication.getCurrentUserId())
                .target_item_id(item.getId()).build()).enqueue(new ApolloCall.Callback<DisLikeMutation.Data>() {
            @Override
            public void onResponse(@NotNull Response<DisLikeMutation.Data> response) {
                PostModel result = listPost.get(item.index - 1);
                result.setIslike(false);
                result.setLike(item.getLike() - 1);
                actionPosition = item.index - 1;
                try {
                    view.action("addLikeSuccess", null, null, null);
                } catch (AppException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {

            }
        });
    }

    public void onRefreshFollow() {
        followsData.clear();
        currentPage = 0;
        isLoadFull = false;
    }

    public void onRefresh(int userId) {
        currentPage = 0;
        isLoadFull = false;
        listPost.clear();
        if (userId != 0) {
            getPosts(userId, 0);
        } else {
            getPostPublic(NewsFeedApplication.getCurrentUserId(), 0);
        }
    }
}
