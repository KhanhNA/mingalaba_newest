package com.nextsolutions.newfeed.view.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.animators.AnimationType;
import com.luck.picture.lib.config.PictureConfig;
import com.luck.picture.lib.config.PictureMimeType;
import com.luck.picture.lib.decoration.GridSpacingItemDecoration;
import com.luck.picture.lib.entity.LocalMedia;
import com.luck.picture.lib.tools.ScreenUtils;
import com.nextsolutions.newfeed.R;
import com.nextsolutions.newfeed.adapter.GridImageAdapter;
import com.nextsolutions.newfeed.adapter.ImagesPostDetailAdapter;
import com.nextsolutions.newfeed.application.NewsFeedApplication;
import com.nextsolutions.newfeed.databinding.ActivityPostDetailBinding;
import com.nextsolutions.newfeed.listener.MyResultCallback;
import com.nextsolutions.newfeed.model.CommentModel;
import com.nextsolutions.newfeed.model.PostModel;
import com.nextsolutions.newfeed.util.IntentConstants;
import com.nextsolutions.newfeed.util.TsUtils;
import com.nextsolutions.newfeed.view.adapter.CommentAdapter;
import com.nextsolutions.newfeed.view.dialog.DialogConfirm;
import com.nextsolutions.newfeed.view.dialog.DialogPostOption;
import com.nextsolutions.newfeed.viewmodel.PostDetailVM;
import com.nextsolutions.newfeed.widget.FullyGridLayoutManager;
import com.nextsolutions.newfeed.widget.glide.GlideEngine;
import com.tsolution.base.BaseActivity;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.listener.AdapterListener;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.view.menu.MenuBuilder;
import androidx.appcompat.view.menu.MenuPopupHelper;
import androidx.appcompat.widget.PopupMenu;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

public class PostDetailActivity extends BaseActivity<ActivityPostDetailBinding> {
    PostDetailVM postDetailVM;
    int itemId;
    private CommentAdapter commentAdapter;
    private GridImageAdapter imageSelectAdapter;
    private ImagesPostDetailAdapter imagesPostDetailAdapter;
    private List<LocalMedia> localMedia;
    private DialogConfirm dialogConfirm;

    public static int PICK_IMAGE_PERMISSIONS_REQUEST_CODE = 999;
    DialogConfirm dialogDelete;
    private static int REQUEST_EDIT_POST = 6262;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        postDetailVM = (PostDetailVM) viewModel;

        setSupportActionBar(binding.toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        if (getIntent() != null && getIntent().getExtras() != null) {
            if (getIntent().hasExtra(IntentConstants.INTENT_ITEM_ID)) {
                itemId = Integer.parseInt(getIntent().getExtras().getString(IntentConstants.INTENT_ITEM_ID, "-1"));
            }
            if (itemId != -1) {
                postDetailVM.getPostById(itemId);
            }
        }
        initAdapter(savedInstanceState);
        initView();
        registerObserver();

    }

    private void registerObserver() {
        postDetailVM.getCommentVM().getClearData().observe(this, isClear -> {
            if (isClear)
                imageSelectAdapter.clearData();
        });

        postDetailVM.getCommentVM().getNotifyChangePosition().observe(this, position -> {
            if(position >= 0) {
                commentAdapter.notifyItemChanged(position);
            }
        });

        postDetailVM.getCommentVM().getListComment().observe(this, list -> {
            commentAdapter.notifyDataSetChanged();
            if (list.size() > 0) {
                binding.emptyComment.setVisibility(View.GONE);
            } else {
                binding.emptyComment.setVisibility(View.VISIBLE);
            }
        });
    }

    @SuppressLint("SetTextI18n")
    private void initView() {
        binding.refresh.setOnRefreshListener(() -> {
            postDetailVM.getPostById(itemId);
        });

        //danh sách ảnh của post
        imagesPostDetailAdapter = new ImagesPostDetailAdapter(this, (v, position) -> {
            previewImage(position);
        });
        binding.rcImagesPost.setAdapter(imagesPostDetailAdapter);
        binding.rcImagesPost.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        SnapHelper snapHelper = new PagerSnapHelper();
        snapHelper.attachToRecyclerView(binding.rcImagesPost);

        //danh sách comment
        commentAdapter = new CommentAdapter(postDetailVM.getCommentVM().getListComment().getValue(), new AdapterListener() {
            @Override
            public void onItemClick(View view, Object o) {
                int id = view.getId();
                if (id == R.id.imgAuthor || id == R.id.txtAuthorName) {
                    Intent intent = new Intent(PostDetailActivity.this, ProfileActivity.class);
                    intent.putExtra("USER_ID", ((CommentModel) o).getUser_id());
                    startActivity(intent);
                } else if (id == R.id.seeMoreComment) {
                    view.findViewById(R.id.txtSeeMore).setVisibility(View.GONE);
                    view.findViewById(R.id.loadingSeeMore).setVisibility(View.VISIBLE);
                    postDetailVM.getCommentVM().getMoreComment(postDetailVM.getPostDetail().get().getId());
                }
            }

            @Override
            public void onItemLongClick(View view, Object o) {
                if (view.getId() == R.id.txtComment) {
                    showCommentOption(view, (CommentModel) o);
                }
            }
        });
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        binding.rcComment.setLayoutManager(layoutManager);
        binding.rcComment.setAdapter(commentAdapter);

        //action like
        binding.btnLike.setOnClickListener(v -> {
            PostModel postModel = postDetailVM.getPostDetail().get();
            if (postModel != null && postModel.getId() != null) {
                if (postModel.getIslike()) {
                    String countLike = postModel.getLike() - 1 > 1 ? postModel.getLike() - 1 + " " + getString(R.string.likes) : postModel.getLike() - 1 + " " + getString(R.string.like);
                    binding.txtLikeCount.setText(countLike);
                    if (postModel.getLike() - 1 <= 0) {
                        binding.txtLikeCount.setVisibility(View.GONE);
                    }
                    postDetailVM.disLike(postModel);
                } else {
                    binding.txtLikeCount.setVisibility(View.VISIBLE);
                    String countLike = postModel.getLike() + 1 > 1 ? postModel.getLike() + 1 + " " + getString(R.string.likes) : postModel.getLike() + 1 + " " + getString(R.string.like);
                    binding.txtLikeCount.setText(countLike);
                    postDetailVM.addLike(postModel);
                }
            }
        });

        //action add comment
        binding.postOption.setOnClickListener(v -> {
            showPostOption(postDetailVM.getPostDetail().get());
        });

        binding.btnAddComment.setOnClickListener(v -> {
            CommentModel commentModel = postDetailVM.getCommentVM().getCmtObs().get();
            assert commentModel != null;

            if (postDetailVM.getCommentVM().getIsEdit().get()) {
                runOnUiThread(() -> {
                    postDetailVM.getCommentVM().getListComment().getValue().set(commentModel.index - 1, commentModel);
                    commentAdapter.notifyItemChanged(commentModel.index - 1);
                    binding.rcComment.smoothScrollToPosition(commentModel.index - 1);
                });
            } else {
                postDetailVM.getPostDetail().get().setLike(postDetailVM.getPostDetail().get().getLike());
                commentModel.setUser_id(NewsFeedApplication.getCurrentUserId());
                commentModel.setType(1);
                commentModel.setUsername(NewsFeedApplication.getCurrentUser().getUsername());
                commentModel.setDisplay_name(NewsFeedApplication.getCurrentUser().getDisplay_name());
                commentModel.setUser_avatar_url(NewsFeedApplication.getCurrentUser().getAvatar());
                postDetailVM.getCommentVM().getListComment().getValue().add(0, commentModel);
                runOnUiThread(() -> {
                    commentAdapter.notifyItemInserted(0);
                    binding.rcComment.smoothScrollToPosition(0);
                });
            }
            commentModel.setMessage(binding.txtAddComment.getText().toString());
            postDetailVM.getCommentVM().upLoadComment(postDetailVM.getPostDetail().get().getId());
            binding.txtAddComment.clearFocus();
            binding.txtAddComment.setText("");

        });
        binding.btnAddImg.setOnClickListener(v -> pickImagesForComment());
        binding.txtAddComment.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                binding.btnAddComment.setVisibility(s.length() > 0 ? View.VISIBLE : View.GONE);
                if (s.length() > 5000) {
                    binding.txtAddComment.setError(getString(R.string.text_too_long));
                    binding.btnAddComment.setEnabled(false);
                } else {
                    binding.txtAddComment.setError(null);
                    binding.btnAddComment.setEnabled(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    private void showPostOption(PostModel postModel) {
        DialogPostOption dialogPostOption = new DialogPostOption(id -> {
            if (id == R.id.btnDelPost) {
                showConfirmDeletePost(postModel);
            } else if (id == R.id.btnEditPost) {
                openEditPost(postModel);
            }
        });
        dialogPostOption.show(getSupportFragmentManager(), dialogPostOption.getTag());
    }

    private void openEditPost(PostModel postModel) {
        Intent intent = new Intent(this, AddPostActivity.class);
        intent.putExtra("POST_MODEL", postModel);
        startActivityForResult(intent, REQUEST_EDIT_POST);
    }

    private void showConfirmDeletePost(PostModel postModel) {
        dialogConfirm = new DialogConfirm(getString(R.string.delete_post), getString(R.string.msg_delete_post), v -> {
            postDetailVM.deletePost(postModel);
            dialogConfirm.dismiss();
            finish();
        });
        dialogConfirm.show(getSupportFragmentManager(), dialogConfirm.getTag());
    }

    @SuppressLint("RestrictedApi")
    private void showCommentOption(View view, CommentModel commentModel) {
        //Creating the instance of PopupMenu
        PopupMenu popup = new PopupMenu(this, view);
        //Inflating the Popup using xml file
        if (commentModel.getUser_id().equals(NewsFeedApplication.getCurrentUserId())) {
            popup.getMenuInflater().inflate(R.menu.menu_option_my_comment, popup.getMenu());
        } else {
            popup.getMenuInflater().inflate(R.menu.menu_option_comment, popup.getMenu());
        }

        MenuPopupHelper menuHelper = new MenuPopupHelper(this, (MenuBuilder) popup.getMenu(), view);
        menuHelper.setForceShowIcon(true);

        popup.setOnMenuItemClickListener(item -> {
            int id = item.getItemId();
            if (id == R.id.action_coppy) {
                ClipboardManager clipboard = (ClipboardManager) this.getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("Good_Boy", commentModel.getMessage());
                clipboard.setPrimaryClip(clip);
                Toast.makeText(this, getResources().getString(R.string.COPIED_TO_CLIPBOARD), Toast.LENGTH_SHORT).show();
            } else if (id == R.id.action_edit) {
                openEditComment(commentModel);
            } else if (id == R.id.action_del) {
                showDialogDelete(commentModel);
            }
            return true;
        });

        menuHelper.show();//showing popup menu
    }

    private void openEditComment(CommentModel commentModel) {
        postDetailVM.getCommentVM().getIsEdit().set(true);
        postDetailVM.getCommentVM().getCmtObs().set(commentModel);
        if (TsUtils.isNotNull(commentModel.getUrl())) {
            List<LocalMedia> localMediaList = new ArrayList<>();
            for (String url : commentModel.getUrl()) {
                LocalMedia localMedia = new LocalMedia();
                localMedia.setPath(url);
                localMediaList.add(localMedia);
            }
            postDetailVM.getCommentVM().setDataFileChoose(localMediaList);
            imageSelectAdapter.setList(localMediaList);
        }
    }

    private void showDialogDelete(CommentModel commentModel) {
        dialogDelete = new DialogConfirm(getString(R.string.delete_comment), "", v -> {
            postDetailVM.getCommentVM().deleteComment(commentModel);
            postDetailVM.getCommentVM().getListComment().getValue().remove(commentModel.index - 1);
            commentAdapter.notifyDataSetChanged();
            dialogDelete.dismiss();
        });
        dialogDelete.show(getSupportFragmentManager(), dialogDelete.getTag());
    }

    //setup adapter for pick image to comment
    private void initAdapter(Bundle savedInstanceState) {
        initLayoutManager();
        imageSelectAdapter = new GridImageAdapter(this, this::pickImage);
        if (savedInstanceState != null && savedInstanceState.getParcelableArrayList("selectorList") != null) {
            imageSelectAdapter.setList(savedInstanceState.getParcelableArrayList("selectorList"));
        }
        imageSelectAdapter.setSelectMax(6);
        imageSelectAdapter.setOnClear(() -> postDetailVM.getCommentVM().getIsNotEmpty().set(false));
        binding.rcImages.setAdapter(imageSelectAdapter);
        imageSelectAdapter.setOnItemClickListener((v, position) -> {
            openOptionImage(position);
        });
        imageSelectAdapter.setItemLongClickListener((holder, position, v) -> {
        });
    }

    private void initLayoutManager() {
        FullyGridLayoutManager manager = new FullyGridLayoutManager(this,
                4, GridLayoutManager.VERTICAL, false);
        binding.rcImages.setLayoutManager(manager);

        binding.rcImages.addItemDecoration(new GridSpacingItemDecoration(4,
                ScreenUtils.dip2px(this, 8), false));
    }

    private void openOptionImage(int position) {
        List<LocalMedia> selectList = imageSelectAdapter.getData();
        if (selectList.size() > 0) {
            LocalMedia media = selectList.get(position);
            String mimeType = media.getMimeType();
            int mediaType = PictureMimeType.getMimeType(mimeType);
            switch (mediaType) {
                case PictureConfig.TYPE_VIDEO:
                    PictureSelector.create(this)
                            .themeStyle(R.style.picture_default_style)
                            .externalPictureVideo(TextUtils.isEmpty(media.getAndroidQToPath()) ? media.getPath() : media.getAndroidQToPath());
                    break;
                default:
                    PictureSelector.create(this)
                            .themeStyle(R.style.picture_default_style)
                            .setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED)
                            .isNotPreviewDownload(true)
                            .loadImageEngine(GlideEngine.createGlideEngine())
                            .openExternalPreview(position, selectList);
                    break;
            }
        }
    }

    private void pickImagesForComment() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                    && checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                    && checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                pickImage();
            } else {
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, PostDetailActivity.PICK_IMAGE_PERMISSIONS_REQUEST_CODE);
            }
        } else {
            pickImage();
        }

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PostDetailActivity.PICK_IMAGE_PERMISSIONS_REQUEST_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                pickImage();
            } else {
                Toast.makeText(this, R.string.permissions_not_granted, Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_EDIT_POST) {
                if (data != null) {
                    PostModel postModel = (PostModel) data.getSerializableExtra("POST_MODEL");
                    if (postModel != null) {
                        postDetailVM.getPostDetail().set(postModel);
                        imagesPostDetailAdapter.notifyDataSetChanged(postModel.getUrl());
                        localMedia = new ArrayList<>();
                        for (String url : postDetailVM.getPostDetail().get().getUrl()) {
                            LocalMedia urlImage = new LocalMedia();
                            urlImage.setPath(url);
                            localMedia.add(urlImage);
                        }
                    }
                }
            }
        }
    }


    private void pickImage() {
        PictureSelector.create(this)
                .openGallery(1)
                .loadImageEngine(GlideEngine.createGlideEngine())
                .theme(R.style.picture_WeChat_style)
                .isWeChatStyle(true)
                .maxSelectNum(6)
                .setRecyclerAnimationMode(AnimationType.SLIDE_IN_BOTTOM_ANIMATION)
                .synOrAsy(true)
                .compress(true)
                .compressQuality(80)
                .selectionMode(PictureConfig.MULTIPLE)
                .selectionMedia(imageSelectAdapter.getData())
                .forResult(new MyResultCallback(imageSelectAdapter, postDetailVM.getCommentVM()));
    }

    @Override
    public void processFromVM(String action, View view, BaseViewModel viewModel, Throwable t) {
        super.processFromVM(action, view, viewModel, t);
        if (action.equals("getPostSuccess")) {
            runOnUiThread(() -> {
                if (TsUtils.isNotNull(postDetailVM.getPostDetail().get().getUrl())) {
                    if (binding.rcImagesPost.getVisibility() != View.VISIBLE) {
                        binding.rcImagesPost.setVisibility(View.VISIBLE);
                    }
                    imagesPostDetailAdapter.notifyDataSetChanged(postDetailVM.getPostDetail().get().getUrl());
                } else {
                    binding.rcImagesPost.setVisibility(View.GONE);
                }
            });

            localMedia = new ArrayList<>();
            for (String url : postDetailVM.getPostDetail().get().getUrl()) {
                LocalMedia urlImage = new LocalMedia();
                urlImage.setPath(url);
                localMedia.add(urlImage);
            }

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getBaseActivity().onBackPressed();
                return true;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        postDetailVM.getCommentVM().getListComment().removeObservers(this);
        postDetailVM.getCommentVM().getClearData().removeObservers(this);
        postDetailVM.getCommentVM().getNotifyChangePosition().removeObservers(this);
    }

    @Override
    public int getLayoutRes() {
        return R.layout.activity_post_detail;
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return PostDetailVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }

    private void previewImage(int position) {
        try {
            PictureSelector.create(this)
                    .themeStyle(R.style.picture_default_style)
                    .isNotPreviewDownload(true)
                    .loadImageEngine(GlideEngine.createGlideEngine())
                    .openExternalPreview(position, localMedia);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
