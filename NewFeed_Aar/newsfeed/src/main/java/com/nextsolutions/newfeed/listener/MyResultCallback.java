package com.nextsolutions.newfeed.listener;

import android.util.Log;

import com.luck.picture.lib.entity.LocalMedia;
import com.luck.picture.lib.listener.OnResultCallbackListener;
import com.nextsolutions.newfeed.adapter.GridImageAdapter;
import com.nextsolutions.newfeed.viewmodel.CommentVM;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.List;

public class MyResultCallback implements OnResultCallbackListener<LocalMedia> {
    private static final String TAG = "AddComment";
    private WeakReference<GridImageAdapter> mAdapterWeakReference;
    private CommentVM commentVM;

    public MyResultCallback(GridImageAdapter adapter, CommentVM commentVM) {
        super();
        this.mAdapterWeakReference = new WeakReference<>(adapter);
        this.commentVM = commentVM;
    }

    @Override
    public void onResult(List<LocalMedia> result) {
        commentVM.setDataFileChoose(result);
        for (LocalMedia media : result) {
            Log.d(TAG, "PATH ORIGIN: " + media.getOriginalPath());
            Log.d(TAG, "PATH: " + media.getCompressPath());
        }
        if (mAdapterWeakReference.get() != null) {
            mAdapterWeakReference.get().setList(result);
            mAdapterWeakReference.get().notifyDataSetChanged();
        }
    }

    @Override
    public void onCancel() {
        Log.i(TAG, "PictureSelector Cancel");
    }
}
