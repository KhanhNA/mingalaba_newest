package com.nextsolutions.newfeed.viewmodel;

import android.annotation.SuppressLint;
import android.app.Application;
import android.util.Log;

import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;
import com.luck.picture.lib.entity.LocalMedia;
import com.nextsolutions.AddPostMutation;
import com.nextsolutions.UpdatePostMutation;
import com.nextsolutions.newfeed.application.NewsFeedApplication;
import com.nextsolutions.newfeed.model.PostModel;
import com.nextsolutions.newfeed.model.dto.OptionDTO;
import com.nextsolutions.newfeed.util.StringUtils;
import com.nextsolutions.newfeed.util.TsFileUtils;
import com.nextsolutions.newfeed.util.TsUtils;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.exceptionHandle.AppException;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;

import io.reactivex.schedulers.Schedulers;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AddPostVM extends BaseViewModel {
    private String TAG = "AddPostVM";
    private ObservableField<List<LocalMedia>> lstFileSelected;
    private List<String> urlFile;
    private ObservableBoolean isNotEmpty;
    private ObservableField<OptionDTO> obOptionDTO;
    private ArrayList<OptionDTO> lstOption;
    private String messageFriendAllView="";
    private String messageFriendAll="";
    private String messageOnlyMe="";
    private String messageOnlyMeView ="";
    private ObservableBoolean isLoading = new ObservableBoolean();

    private ObservableField<PostModel> postObs = new ObservableField<>();

    public AddPostVM(@NonNull Application application) {
        super(application);
        lstOption = new ArrayList<>();
        lstFileSelected = new ObservableField<>();
        lstFileSelected.set(new ArrayList<>());
        isNotEmpty = new ObservableBoolean();
        obOptionDTO = new ObservableField<>();
        isNotEmpty.set(false);
        urlFile = new ArrayList<>();
        postObs.set(new PostModel());
    }

    public void uploadPost(boolean isEdit ,List<String> lstUrl) {
        postObs.get().setUrl(lstUrl);
        if(isEdit){
            editPost();
        }else {
            addPost();
        }
    }

    private void addPost() {
        isLoading.set(true);
        PostModel postModel = postObs.get();
        assert postModel != null;
        NewsFeedApplication.getInstance().getApi().mutate(AddPostMutation.builder().user_id(NewsFeedApplication.getCurrentUserId())
                .message(postModel.getMessage())
                .description(postModel.getDescription())
                .url(TsUtils.isNotNull(postModel.getUrl()) ? postModel.getUrl() : null)
                .type(1)
                .isPrivate(obOptionDTO.get().getIsOnlyMe())
                .string_color(postModel.getString_color())
                .string_font(postModel.getString_font())
                .build()).enqueue(new ApolloCall.Callback<AddPostMutation.Data>() {
            @Override
            public void onResponse(@NotNull Response<AddPostMutation.Data> response) {
                Log.d(TAG, response.toString());
                try {
                    view.action("uploadPostSuccess", null, null, null);
                } catch (AppException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {
                Log.d(TAG, e.getMessage());
                try {
                    isLoading.set(false);
                    view.action("uploadPostFail", null, null, null);
                } catch (AppException ex) {
                    ex.printStackTrace();
                }
            }
        });
    }

    public void editPost() {
        isLoading.set(true);

        PostModel postModel = postObs.get();
        assert postModel != null;
        NewsFeedApplication.getInstance().getApi().mutate(UpdatePostMutation.builder().itemId(postModel.getId())
                .message(postModel.getMessage())
                .description(postModel.getDescription())
                .url(postModel.getUrl())
                .type(1)
                .isPrivate(obOptionDTO.get().getIsOnlyMe())
                .string_color(postModel.getString_color())
                .string_font(postModel.getString_font())
                .build()).enqueue(new ApolloCall.Callback<UpdatePostMutation.Data>() {
            @Override
            public void onResponse(@NotNull Response<UpdatePostMutation.Data> response) {
                Log.d(TAG, response.toString());
                try {
                    isLoading.set(false);
                    view.action("uploadPostSuccess", null, null, null);
                } catch (AppException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {
                Log.d(TAG, e.getMessage());
                try {
                    isLoading.set(false);
                    view.action("uploadPostFail", null, null, null);
                } catch (AppException ex) {
                    ex.printStackTrace();
                }
            }
        });
    }


    public void setDataFileChoose(List<LocalMedia> albumFiles) {
        lstFileSelected.set(albumFiles);
        isNotEmpty.set(TsUtils.isNotNull(lstFileSelected.get()));
    }


    @SuppressLint("CheckResult")
    public void uploadPostWithFile(boolean isEdit) {
        isLoading.set(true);
        TsFileUtils tsFileUtils = new TsFileUtils();
        Map<File, String> mapFile = new HashMap<>();
        List<String> urls = new ArrayList<>();
        for (LocalMedia file : lstFileSelected.get()) {
            if(!StringUtils.isNullOrEmpty(file.getCompressPath())) {//nếu không có compressPath tức là ảnh lấy từ bài viết đang edit.
                File fileUpload = new File(file.getCompressPath());
                mapFile.put(fileUpload, fileUpload.getName());
                Log.d(TAG, fileUpload.getName());
            }else {
                urls.add(file.getPath());
            }
        }
        tsFileUtils.uploadMultiple(mapFile)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe(() -> {
                    Log.d(TAG, "Uploads completed!");
                    for (Map.Entry<File, String> entry : mapFile.entrySet()) {
                        urlFile.add(NewsFeedApplication.BASE_URL_FILE + entry.getValue());
                    }
                    urls.addAll(urlFile);
                    uploadPost(isEdit, urls);
                });
    }

    public List<OptionDTO> getLstPermissionView(boolean isPrivate) {
        if (lstOption == null || lstOption.size() == 0) {
            OptionDTO option1 = new OptionDTO(messageFriendAll, messageFriendAllView);
            option1.setIsOnlyMe(false);

            OptionDTO option2 = new OptionDTO(messageOnlyMe, messageOnlyMeView);
            option2.setIsOnlyMe(true);

            lstOption.add(option1);
            lstOption.add(option2);

        }

        if(!isPrivate){
            lstOption.get(0).checked = true;
            lstOption.get(1).checked = false;
            obOptionDTO.set(lstOption.get(0));
        }else {
            lstOption.get(0).checked = false;
            lstOption.get(1).checked = true;
            obOptionDTO.set(lstOption.get(1));
        }

        return lstOption;
    }
}
