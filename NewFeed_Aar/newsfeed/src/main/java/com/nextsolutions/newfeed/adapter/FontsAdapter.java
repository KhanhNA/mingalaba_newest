package com.nextsolutions.newfeed.adapter;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nextsolutions.newfeed.R;
import com.nextsolutions.newfeed.listener.OnItemClickListener;
import com.tsolution.base.listener.AdapterListener;

import java.util.List;

public class FontsAdapter extends RecyclerView.Adapter<FontsAdapter.ViewHolder> {
    LayoutInflater inflater;
    AssetManager assetManager;
    public List<String> fonts;
    Context context;

    public String selectedColor = "#2d2d2d";
    public int selected = -1;

    private OnItemClickListener mItemClickListener;

    public FontsAdapter(Context context, List<String> fonts, OnItemClickListener itemClickListener) {
        assetManager = context.getAssets();
        this.context = context;
        this.mItemClickListener = itemClickListener;
        inflater = LayoutInflater.from(context);
        this.fonts = fonts;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View fontView = inflater.inflate(R.layout.item_fonts, parent, false);
        return new ViewHolder(fontView, mItemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if (selected != -1 && selected == position) {
            holder.fonts.setBackground(context.getResources().getDrawable(R.drawable.edit_text_desgin_active));
            holder.fonts.setTextColor((Color.parseColor(selectedColor)));
        } else {
            holder.fonts.setBackground(context.getResources().getDrawable(R.drawable.edit_text_desgin_gray));
            holder.fonts.setTextColor((Color.parseColor("#2d2d2d")));
        }

        String font = fonts.get(position);
        holder.fonts.setText(font);
        Typeface type = Typeface.createFromAsset(assetManager, "fonts/" + font);
        holder.fonts.setTypeface(type);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return fonts != null ? fonts.size() : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private final OnItemClickListener mItemClickListener;
        TextView fonts;

        public ViewHolder(@NonNull View itemView, OnItemClickListener mItemClickListener) {
            super(itemView);
            this.mItemClickListener = mItemClickListener;
            fonts = itemView.findViewById(R.id.fonts);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mItemClickListener != null) {
                selected = getAdapterPosition();
                mItemClickListener.onItemClick(v, getAdapterPosition());
            }
        }
    }
}
