package com.nextsolutions.newfeed.model;

import com.nextsolutions.SearchUserQuery;
import com.tsolution.base.BaseModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PeopleModel extends BaseModel {
    private int id;
    private String username;
    private String email;
    private String avatar;
    private String banner;
    private String display_name;

    public void setData(SearchUserQuery.SearchUser item) {
        this.id = item.id();
        this.username = item.username();
        this.avatar = item.avatar();
        this.email = item.email();
        this.display_name = item.display_name();
    }

    public String getDisplayName() {
        return display_name != null && !display_name.isEmpty() ? display_name : username;
    }
}
