package com.nextsolutions.newfeed.model;

import android.widget.TextView;

import com.nextsolutions.newfeed.R;
import com.tsolution.base.BaseModel;

import java.util.Date;
import java.util.List;

import androidx.databinding.BindingAdapter;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PostModel extends BaseModel {
    Integer id;
    Integer user_id;
    String user_avatar_url;
    String username;
    Boolean islike;
    int like;
    int type;
    String message;
    List<String> url;
    String description;
    Date createdAt;
    Date updatedAt;
    Integer comment_number;
    String string_font;
    String string_color;
    String display_name;
    boolean isprivate;

    public String getDisplayName() {
        return display_name != null && !display_name.isEmpty() ? display_name : username;
    }

    @BindingAdapter("likeCount")
    public static void likeCount(TextView textView, int like ){
        String countLike = like > 1 ? like + " " + textView.getContext().getString(R.string.likes)
                : like + " " + textView.getContext().getString(R.string.like);
        textView.setText(countLike);
    }
}
