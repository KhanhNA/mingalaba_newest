package com.nextsolutions.newfeed.view.viewHolder;

import com.tsolution.base.BR;
import com.tsolution.base.listener.AdapterListener;

import androidx.annotation.NonNull;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;

public class ExampleViewHolder extends RecyclerView.ViewHolder {
    private ViewDataBinding binding;

    public ExampleViewHolder(@NonNull ViewDataBinding itemView) {
        super(itemView.getRoot());
        this.binding = itemView;
    }

    public void bind(AdapterListener listener){
        binding.setVariable(BR.listenerAdapter, listener);
    }
}
