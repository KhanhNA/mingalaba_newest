package com.nextsolutions.newfeed.application;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.ApolloClient;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;
import com.nextsolutions.CreateUserMutation;
import com.nextsolutions.LoginMutation;
import com.nextsolutions.UpdateUserMutation;
import com.nextsolutions.newfeed.listener.CreateAccountListener;
import com.nextsolutions.newfeed.listener.LoginCallback;
import com.nextsolutions.newfeed.model.PeopleModel;
import com.nextsolutions.newfeed.util.StringUtils;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;

public class NewsFeedApplication extends Application {
    public static String BASE_URL = "http://dev.nextsolutions.com.vn:5000";
    public static final String BASE_URL_FILE = "https://newfeedd7ae5f4f39bf4299ab3e389acff1706f165400-android.s3-ap-southeast-1.amazonaws.com/public/";
    public static final String ACCESS_KEY = "AKIAIGE4MILHWHYNRL2A";
    public static final String SECRET_KEY = "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";
    private ApolloClient apolloClient;
    private static NewsFeedApplication mInstance;
    HashMap<String, Object> clientCache;
    public static final String CACHE_USER = "CACHE_USER";
    public static String JSON_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    public static String TOKEN_USER = "";
    private static Context mContext;

    @Override
    public void onCreate() {
        super.onCreate();
        //conmment mContext = this; khi build thành thư viện
//        mContext = this;
        mInstance = this;
        clientCache = new HashMap<>();
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(chain -> {
                    Request original = chain.request();
                    Request.Builder builder = original.newBuilder().method(original.method(), original.body());
                    builder.header("Authorization", "Bearer " + TOKEN_USER);
                    return chain.proceed(builder.build());
                })
                .build();
        apolloClient = ApolloClient.builder()
                .serverUrl(BASE_URL)
                .okHttpClient(okHttpClient)
                .build();
//        getProfileUser("haint", "abc@123");
    }

    public void createAccount(String userName, CreateAccountListener createAccountListener) {
        apolloClient.mutate(CreateUserMutation.builder().username(userName).password("abc@123").build()).enqueue(new ApolloCall.Callback<CreateUserMutation.Data>() {
            @Override
            public void onResponse(@NotNull Response<CreateUserMutation.Data> response) {
                if (response.data() != null) {
                    createAccountListener.createUserSuccess();
                } else {
                    createAccountListener.createUserError();
                }

            }

            @Override
            public void onFailure(@NotNull ApolloException e) {
                createAccountListener.createUserSuccess();
            }
        });
    }

    public static Context getContext() {
        return mContext;
    }

    /**
     * @param c application context
     * @param baseUrl url to server
     */
    public void setContext(Context c, String baseUrl) {
        mContext = c;
        BASE_URL = baseUrl;
    }
    /**
     * @param c application context
     */
    public void setContext(Context c) {
        mContext = c;
    }

    public void getProfileUser(String userName, String passWord, String type, LoginCallback loginCallback) {
        // Login
        LoginMutation.Builder loginMutation = LoginMutation.builder()
                .username(userName)
                .password(passWord);
        if (StringUtils.isNotNullAndNotEmpty(type)) {
            loginMutation.type(type);
        }
        NewsFeedApplication.getInstance().getApi().mutate(loginMutation.build()).enqueue(new ApolloCall.Callback<LoginMutation.Data>() {
            @Override
            public void onResponse(@NotNull Response<LoginMutation.Data> response) {
                if (response.data() != null && response.data().login() != null) {
                    String token = Objects.requireNonNull(response.data().login()).jwt();
                    if (StringUtils.isNotNullAndNotEmpty(token)) {
                        OkHttpClient.Builder okHttpClient = new OkHttpClient.Builder()
                                .connectTimeout(30, TimeUnit.SECONDS)
                                .writeTimeout(30, TimeUnit.SECONDS)
                                .readTimeout(30, TimeUnit.SECONDS)
                                .addInterceptor(chain -> {
                                    Request request = chain.request();
                                    Request.Builder newRequest = request.newBuilder().addHeader("Authorization", "Bearer " + token);
                                    return chain.proceed(newRequest.build());
                                });
                        NewsFeedApplication.getInstance().setOkHttp(okHttpClient.build());
                        //
                        PeopleModel user = new PeopleModel();
                        user.setAvatar(response.data().login().avatar());
                        user.setId(response.data().login().id());
                        user.setUsername(response.data().login().username());
                        user.setDisplay_name(response.data().login().display_name() != null ? response.data().login().display_name() : response.data().login().username());
                        user.setBanner(response.data().login().banner_url());
                        NewsFeedApplication.getInstance().putCache(NewsFeedApplication.CACHE_USER, user);
                        loginCallback.loginSuccess(user);
                    }
                }else {
                    loginCallback.loginError("loginFail");
                }
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {
                loginCallback.loginError(e.getMessage());
                Log.d("login", e.getMessage());
            }
        });

    }

    public void updateTokenFb(String userName, String token) {
        UpdateUserMutation updateUser
                = UpdateUserMutation.builder()
                .fcm_device_token(token)
                .username(userName).build();
        NewsFeedApplication.getInstance().getApi().mutate(updateUser).enqueue(new ApolloCall.Callback<UpdateUserMutation.Data>() {
            @Override
            public void onResponse(@NotNull Response<UpdateUserMutation.Data> response) {
                Log.d("Upload", "Success");
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {
                Log.d("Upload", "Error: " + e.getMessage());
            }
        });
    }

    public void setOkHttp(OkHttpClient okHttp) {
        apolloClient = ApolloClient.builder()
                .serverUrl(BASE_URL)
                .okHttpClient(okHttp)
                .build();
    }

    public static synchronized NewsFeedApplication getInstance() {
        return mInstance;
    }

    public ApolloClient getApi() {
        return apolloClient;
    }

    public void putCache(String key, Object obj) {
        if (clientCache == null) {
            clientCache = new HashMap<>();
        }
        clientCache.put(key, obj);
    }

    public Object getFromCache(String key) {
        if (clientCache == null) {
            return null;
        }
        return clientCache.get(key);
    }

    public static int getCurrentUserId() {
        return getCurrentUser() != null ? getCurrentUser().getId() : 0;
    }

    public static PeopleModel getCurrentUser() {
        return (PeopleModel) NewsFeedApplication.getInstance().getFromCache(NewsFeedApplication.CACHE_USER);
    }

}
