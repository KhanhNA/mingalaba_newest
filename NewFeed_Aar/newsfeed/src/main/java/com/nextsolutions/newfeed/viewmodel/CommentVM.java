package com.nextsolutions.newfeed.viewmodel;

import android.annotation.SuppressLint;
import android.app.Application;
import android.util.Log;

import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.luck.picture.lib.entity.LocalMedia;
import com.luck.picture.lib.tools.ToastUtils;
import com.nextsolutions.AddCommentMutation;
import com.nextsolutions.DeleteCommentMutation;
import com.nextsolutions.DeletePostMutation;
import com.nextsolutions.ItemCommentQuery;
import com.nextsolutions.OneItemQuery;
import com.nextsolutions.UpdateCommentMutation;
import com.nextsolutions.UpdatePostMutation;
import com.nextsolutions.newfeed.application.NewsFeedApplication;
import com.nextsolutions.newfeed.model.CommentModel;
import com.nextsolutions.newfeed.model.PostModel;
import com.nextsolutions.newfeed.util.GsonUTCDateAdapter;
import com.nextsolutions.newfeed.util.IntentConstants;
import com.nextsolutions.newfeed.util.StringUtils;
import com.nextsolutions.newfeed.util.TsFileUtils;
import com.nextsolutions.newfeed.util.TsUtils;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.exceptionHandle.AppException;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import androidx.lifecycle.MutableLiveData;

import io.reactivex.schedulers.Schedulers;
import lombok.Getter;
import lombok.Setter;

/**
 * create by Good_Boy 25/04/2020
 */
@Getter
@Setter
public class CommentVM extends BaseViewModel<CommentModel> {
    private ObservableBoolean isLoading = new ObservableBoolean(false);
    private ObservableBoolean isNotEmpty;
    private int currentPage;
    private int totalPage;
    private MutableLiveData<List<CommentModel>> listComment = new MutableLiveData<>();
    private MutableLiveData<Integer> notifyChangePosition = new MutableLiveData<>();
    private ObservableField<List<LocalMedia>> lstFileSelected;

    private MutableLiveData<Boolean> clearData;
    private ObservableField<PostModel> post = new ObservableField<>();
    AddCommentMutation addCommentMutation;

    private int totalComment;
    private ObservableField<CommentModel> cmtObs = new ObservableField<>();

    private ObservableBoolean isEdit = new ObservableBoolean();

    public CommentVM(@NonNull Application application) {
        super(application);
        listComment.setValue(new ArrayList<>());
        isNotEmpty = new ObservableBoolean(false);
        clearData = new MutableLiveData<>();
        lstFileSelected = new ObservableField<>();
        lstFileSelected.set(new ArrayList<>());
        notifyChangePosition.postValue(-1);
        cmtObs.set(new CommentModel());
    }


    public void getComments(int postId, int page_number) {
        if(page_number == 0){
            listComment.getValue().clear();
            currentPage = 0;
        }
        isLoading.set(true);
        NewsFeedApplication.getInstance().getApi().query(ItemCommentQuery.builder()
                .itemId(postId)
                .userId(NewsFeedApplication.getCurrentUserId())
                .page_number(page_number).build()
        ).enqueue(new ApolloCall.Callback<ItemCommentQuery.Data>() {
            @Override
            public void onResponse(@NotNull Response<ItemCommentQuery.Data> response) {
                isLoading.set(false);
                if (response.data().getComments() != null && response.data().getComments().size() > 0) {
                    currentPage++;
                    List<CommentModel> comments = listComment.getValue();
                    totalComment = response.data().getComments().get(0).total();
                    totalPage = totalComment / 10;
                    totalPage += totalComment % 10 > 0 ? 1 : 0;

                    //xóa phần từ null ở cuối danh sách
                    if (comments != null && comments.size() > 0) {
                        comments.remove(comments.size() - 1);
                    }
                    //danh sách bình luận đã được khởi tạo trên constructor
                    assert comments != null;
                    comments.addAll(convertData(response.data().getComments()));

                    //thêm phần tử null thể hiện nút xem thêm.
                    if (currentPage == totalPage) {
                    } else {
                        comments.add(null);
                    }
                    listComment.postValue(comments);
                }
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {
                isLoading.set(false);
            }
        });
    }

    public void getMoreComment(int postId) {
        if (currentPage <= totalPage) {
            getComments(postId, currentPage);
        }
    }

    public void getPostById() {
        NewsFeedApplication.getInstance().getApi().query(OneItemQuery.builder()
                .userId(post.get().getUser_id())
                .itemId(post.get().getId()).build()
        ).enqueue(new ApolloCall.Callback<OneItemQuery.Data>() {
            @Override
            public void onResponse(@NotNull Response<OneItemQuery.Data> response) {
                if (response.data() != null && response.data().oneItem() != null) {
                    GsonBuilder jsonBuilder = new GsonBuilder();
                    jsonBuilder.registerTypeAdapter(Date.class, new GsonUTCDateAdapter());
                    Gson gson = jsonBuilder.create();
                    String json = gson.toJson(response.data().oneItem());
                    PostModel rs = gson.
                            fromJson(json, PostModel.class);
                    post.set(rs);
                }
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {
            }
        });
    }


    @SuppressLint("CheckResult")
    public void upLoadComment(int targetId) {
        CommentModel cmt = cmtObs.get();
        isLoading.set(true);
        cmt.setUrl(null);
        List<String> urlFile = new ArrayList<>();
        List<String> urlCmt = new ArrayList<>();

        isNotEmpty.set(false);
        Map<File, String> mapFile = new HashMap<>();

        for (LocalMedia file : lstFileSelected.get()) {
            if (!StringUtils.isNullOrEmpty(file.getCompressPath())) {//nếu không có compressPath tức là ảnh lấy từ bài viết đang edit.
                File fileUpload = new File(file.getCompressPath());
                mapFile.put(fileUpload, fileUpload.getName());
            } else {
                urlCmt.add(file.getPath());
            }
        }
        if (mapFile.size() > 0) {
            TsFileUtils tsFileUtils = new TsFileUtils();
            tsFileUtils.uploadMultiple(mapFile)
                    .subscribeOn(Schedulers.io())
                    .observeOn(Schedulers.io())
                    .subscribe(() -> {
                        for (Map.Entry<File, String> entry : mapFile.entrySet()) {
                            urlFile.add(NewsFeedApplication.BASE_URL_FILE + entry.getValue());
                        }
                        urlCmt.addAll(urlFile);
                        cmt.setUrl(urlCmt);
                        if (isEdit.get()) {
                            callApiEditComment();
                        } else {
                            callApiAddComment(targetId);
                        }
                    });
        } else {
            if (isEdit.get()) {
                cmt.setUrl(urlCmt);
                callApiEditComment();
            } else {
                callApiAddComment(targetId);
            }
        }

    }

    private void callApiEditComment() {
        isEdit.set(false);
        CommentModel cmt = cmtObs.get();
        assert cmt != null;
        NewsFeedApplication.getInstance().getApi().mutate(UpdateCommentMutation.builder().itemId(cmt.getId())
                .message(cmt.getMessage())
                .url(cmt.getUrl())
                .build()).enqueue(new ApolloCall.Callback<UpdateCommentMutation.Data>() {
            @Override
            public void onResponse(@NotNull Response<UpdateCommentMutation.Data> response) {
                Log.d("UPLOAD COMMENT", response.data().toString());
                cmtObs.set(new CommentModel());
                isLoading.set(false);
                lstFileSelected.get().clear();
                notifyChangePosition.postValue(cmt.index - 1);


            }

            @Override
            public void onFailure(@NotNull ApolloException e) {
                Log.d("UPLOAD COMMENT", e.getMessage());
                isLoading.set(false);
                lstFileSelected.get().clear();

            }
        });
    }

    private void callApiAddComment(int targetId) {
        CommentModel cmt = cmtObs.get();
        if (TsUtils.isNotNull(cmt.getUrl())) {
            addCommentMutation = AddCommentMutation.builder()
                    .user_id(cmt.getUser_id())
                    .target_item_id(targetId)
                    .message(cmt.getMessage())
                    .url(cmt.getUrl())
                    .type(cmt.getType()).build();
        } else {
            addCommentMutation = AddCommentMutation.builder()
                    .user_id(cmt.getUser_id())
                    .target_item_id(targetId)
                    .message(cmt.getMessage())
                    .type(cmt.getType()).build();
        }
        NewsFeedApplication.getInstance().getApi().mutate(addCommentMutation).enqueue(new ApolloCall.Callback<AddCommentMutation.Data>() {
            @Override
            public void onResponse(@NotNull Response<AddCommentMutation.Data> response) {
                //set id comment cho comment vừa được add trên giao diện
                if (response.data().addComment() != null) {
                    cmt.setId(response.data().addComment().id());
                    try {
                        cmt.setCreatedAt(StringUtils.stringToDate((String) response.data().addComment().createdAt(), NewsFeedApplication.JSON_DATE_FORMAT, Locale.US));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    notifyChangePosition.postValue(0);
                }
                isLoading.set(false);
                clearData.postValue(true);
                lstFileSelected.get().clear();
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {
                isLoading.set(false);
                isNotEmpty.set(true);
                lstFileSelected.get().clear();
            }
        });
    }

    public void deleteComment(CommentModel commentModel) {
        NewsFeedApplication.getInstance().getApi().mutate(DeleteCommentMutation.builder().itemId(commentModel.getId()).build()).enqueue(new ApolloCall.Callback<DeleteCommentMutation.Data>() {
            @Override
            public void onResponse(@NotNull Response<DeleteCommentMutation.Data> response) {
//                try {
//                    view.action(IntentConstants.DELETE_SUCCESS, null, null, null);
//                } catch (AppException e) {
//                    e.printStackTrace();
//                }
                Log.e("DELETE COMMENT", "success");

            }

            @Override
            public void onFailure(@NotNull ApolloException e) {
//                try {
//                    view.action(IntentConstants.UNFOLLOW_FAIL, null, null, null);
//                } catch (AppException ex) {
//                    ex.printStackTrace();
//                }
                Log.e("DELETE COMMENT", "failure");

            }
        });
    }


    /**
     * convert từ data trả về sang đối tượng CommentVM để xử lý
     *
     * @param items danh sách comment từ server
     * @return danh sách comment đã được convert
     */
    private List<CommentModel> convertData(List<ItemCommentQuery.GetComment> items) {
        GsonBuilder jsonBuilder = new GsonBuilder();
        jsonBuilder.registerTypeAdapter(Date.class, new GsonUTCDateAdapter());
        Gson gson = jsonBuilder.create();
        List<CommentModel> result = new ArrayList<>();
        for (ItemCommentQuery.GetComment item : items) {
            String json = gson.toJson(item);
            CommentModel comment = gson.
                    fromJson(json, CommentModel.class);
            result.add(comment);
        }
        return result;
    }

    public void setDataFileChoose(List<LocalMedia> result) {
        lstFileSelected.set(result);
        isNotEmpty.set(TsUtils.isNotNull(result));
    }

}
