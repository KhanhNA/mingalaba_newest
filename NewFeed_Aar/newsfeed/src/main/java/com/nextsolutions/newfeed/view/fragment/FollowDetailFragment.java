package com.nextsolutions.newfeed.view.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.jcodecraeer.xrecyclerview.XRecyclerView;
import com.nextsolutions.newfeed.R;
import com.nextsolutions.newfeed.adapter.XBaseAdapter;
import com.nextsolutions.newfeed.application.NewsFeedApplication;
import com.nextsolutions.newfeed.databinding.FragmentFollowDetailBinding;
import com.nextsolutions.newfeed.model.PeopleModel;
import com.nextsolutions.newfeed.util.IntentConstants;
import com.nextsolutions.newfeed.view.activity.ProfileActivity;
import com.nextsolutions.newfeed.viewmodel.PostVM;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.listener.AdapterListener;

public class FollowDetailFragment extends BaseCustomFragment<FragmentFollowDetailBinding> implements AdapterListener {
    boolean isFollowing;
    int userId;
    private PostVM postVM;
    private XBaseAdapter followsAdapter;

    public FollowDetailFragment(Boolean isFollowing, int userId) {
        this.isFollowing = isFollowing;
        this.userId = userId;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_follow_detail;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        postVM = (PostVM) viewModel;
        if (isFollowing) {
            // Following
            postVM.getFollows(userId, 0);
        } else {
            // Follower
            postVM.getFollowsMe(userId, 0);
        }

        binding.rcFollows.getDefaultRefreshHeaderView();
        followsAdapter = new XBaseAdapter(R.layout.item_user, postVM.followsData, this);
        followsAdapter.setConfigXRecycler(binding.rcFollows, 1);
        binding.rcFollows.setLoadingListener(new XRecyclerView.LoadingListener() {
            @Override
            public void onRefresh() {
                followsAdapter.notifyDataSetChanged();
                postVM.onRefreshFollow();
                if (isFollowing) {
                    // Following
                    postVM.getFollows(userId, 0);
                } else {
                    // Follower
                    postVM.getFollowsMe(userId, 0);
                }
            }

            @Override
            public void onLoadMore() {
                if (isFollowing) {
                    // Following
                    postVM.getMoreFollowing(userId);
                } else {
                    // Follower
                    postVM.getMoreFollowMe(userId);
                }
            }
        });
        binding.rcFollows.setAdapter(followsAdapter);
        return binding.getRoot();
    }

    @Override
    public void processFromVM(String action, View view, BaseViewModel viewModel, Throwable t) {
        super.processFromVM(action, view, viewModel, t);
        switch (action) {
            case "getFollowSuccess":
                requireActivity().runOnUiThread(() -> {
                    binding.rcFollows.refreshComplete();
                    followsAdapter.notifyDataSetChanged();
                });
                break;
            case "getFollowError":
                requireActivity().runOnUiThread(() -> {
                    binding.rcFollows.refreshComplete();
                    binding.rcFollows.setNoMore(true);
                });
            case "noMore":
                requireActivity().runOnUiThread(() -> {
                    binding.rcFollows.setNoMore(true);
                });
                break;
        }
    }

    @Override
    public Class<? extends BaseViewModel> getVMClass() {
        return PostVM.class;
    }

    @Override
    public int getRecycleResId() {
        return 0;
    }

    @Override
    public void onItemClick(View view, Object o) {
        Intent intent = new Intent(requireContext(), ProfileActivity.class);
        intent.putExtra(IntentConstants.INTENT_USER_ID, ((PeopleModel) o).getId());
        startActivity(intent);
    }

    @Override
    public void onItemLongClick(View view, Object o) {

    }
}
