package com.nextsolutions.newfeed.view.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nextsolutions.ItemQuery;
import com.nextsolutions.newfeed.R;
import com.nextsolutions.newfeed.model.PostModel;
import com.nextsolutions.newfeed.view.viewHolder.ExampleViewHolder;
import com.nextsolutions.newfeed.view.viewHolder.PostImageViewHolder;
import com.nextsolutions.newfeed.view.viewHolder.VideoViewHolder;
import com.tsolution.base.BaseModel;
import com.tsolution.base.listener.AdapterListener;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;

public class FeedAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements AdapterListener {
    private AdapterListener adapterListener;
    private List<PostModel> data;

    public FeedAdapter(List lst, AdapterListener listener) {
        this.data = lst;
        this.adapterListener = listener;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        int layoutItem = 0;
        ViewDataBinding binding;
        //1- image, 2- video, 3- url
        switch (viewType){
            case 0:
            case 1:
                layoutItem = R.layout.item_post_imgs;
                binding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), layoutItem, viewGroup, false);
                return new PostImageViewHolder(binding);
            case 2:
                layoutItem = R.layout.item_post_video;
                binding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), layoutItem, viewGroup, false);
                return new ExampleViewHolder(binding);
        }
        layoutItem = R.layout.item_example;
        binding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), layoutItem, viewGroup, false);
        return new ExampleViewHolder(binding);

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {
        PostModel item = data.get(position);
        item.index = position + 1;
        switch (item.getType()){
            case 0:
            case 1:
                ((PostImageViewHolder)viewHolder).bind(item, adapterListener);
                break;
            case 2:
                ((VideoViewHolder)viewHolder).bind(item, adapterListener);
        }
    }


    @Override
    public int getItemCount() {
        return this.data == null ? 0 : (this.data).size();
    }

    public void onItemClick(View v, Object o) {
        if (adapterListener != null) {
            adapterListener.onItemClick(v, o);
        }
    }

    @Override
    public void onItemLongClick(View view, Object o) {

    }

    @Override
    public int getItemViewType(int position) {
        PostModel item = data.get(position);
        return item.getType();
    }

    @Override
    public long getItemId(int position) {
        return data.get(position).getId();
    }


}
