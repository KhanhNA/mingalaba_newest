package com.nextsolutions.newfeed.listener;

public interface CreateAccountListener {
    void createUserSuccess();

    void createUserError();
}
