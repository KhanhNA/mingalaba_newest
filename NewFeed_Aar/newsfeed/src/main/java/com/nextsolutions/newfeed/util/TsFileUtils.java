package com.nextsolutions.newfeed.util;

import android.content.Context;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.mobile.client.AWSMobileClient;
import com.amazonaws.mobile.client.Callback;
import com.amazonaws.mobile.client.UserStateDetails;
import com.amazonaws.mobile.config.AWSConfiguration;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.nextsolutions.newfeed.application.NewsFeedApplication;

import org.json.JSONException;

import java.io.File;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;

public class TsFileUtils {

    private AmazonS3Client sS3Client;
    private AWSCredentialsProvider sMobileClient;
    private BasicAWSCredentials credentials;
    private TransferUtility transferUtility;
    private AWSConfiguration configuration;

    public TsFileUtils() {
        transferUtility();
    }

    private Completable uploadOne(TransferUtility transferUtility, File aLocalFile, String toRemoteKey) {
        return Completable.create(emitter -> {
            transferUtility.upload("public/" + toRemoteKey, aLocalFile, CannedAccessControlList.PublicRead).setTransferListener(new TransferListener() {
                @Override
                public void onStateChanged(int id, TransferState state) {
                    if (TransferState.FAILED.equals(state)) {
                        emitter.onError(new Exception("Transfer state was FAILED."));
                    } else if (TransferState.COMPLETED.equals(state)) {
                        emitter.onComplete();
                    }
                }

                @Override
                public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                }

                @Override
                public void onError(int id, Exception exception) {
                    emitter.onError(exception);
                }
            });
        });

    }

    private AWSCredentialsProvider getCredProvider(Context context) {
        if (sMobileClient == null) {
            final CountDownLatch latch = new CountDownLatch(1);
            AWSMobileClient.getInstance().initialize(context, new Callback<UserStateDetails>() {
                @Override
                public void onResult(UserStateDetails result) {
                    latch.countDown();
                }

                @Override
                public void onError(Exception e) {
                    e.printStackTrace();
                    latch.countDown();
                }
            });
            try {
                latch.await();
                sMobileClient = AWSMobileClient.getInstance();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return sMobileClient;
    }

    public Completable uploadMultiple(Map<File, String> fileToKeyUploads) {
        return transferUtility()
                .flatMapCompletable(transferUtility ->
                        Observable.fromIterable(fileToKeyUploads.entrySet())
                                .flatMapCompletable(entry -> uploadOne(transferUtility, entry.getKey(), entry.getValue()))
                );
    }


    public Single<TransferUtility> transferUtility() {
        if (transferUtility == null) {
            if (configuration == null) {
                configuration = new AWSConfiguration(NewsFeedApplication.getContext());
            }

            return s3Client(configuration).map(s3 ->
                    transferUtility = TransferUtility.builder()
                            .context(NewsFeedApplication.getContext())
                            .s3Client(s3)
                            .awsConfiguration(configuration)
                            .build()
            );
        } else {
            if (configuration == null) {
                configuration = new AWSConfiguration(NewsFeedApplication.getContext());
            }

            return s3Client(configuration).map(s3 ->
                    transferUtility
            );
        }

    }

    public Single<AmazonS3Client> s3Client(AWSConfiguration configuration) {
        return credentialsProvider().map(credentialsProvider -> {
            String regionInConfig = configuration
                    .optJsonObject("S3TransferUtility")
                    .getString("Region");
            Region region = Region.getRegion(regionInConfig);

            return new AmazonS3Client(getBasicCredentials(), region);
        });
    }

    private BasicAWSCredentials getBasicCredentials() {
        if (credentials == null) {
            credentials = new BasicAWSCredentials(NewsFeedApplication.ACCESS_KEY, NewsFeedApplication.SECRET_KEY);
        }
        return credentials;

    }

    public Single<AWSMobileClient> credentialsProvider() {
        return Single.create(emitter -> {
            AWSMobileClient.getInstance().initialize(NewsFeedApplication.getContext(), new Callback<UserStateDetails>() {
                @Override
                public void onResult(UserStateDetails result) {
                    emitter.onSuccess(AWSMobileClient.getInstance());
                }

                @Override
                public void onError(Exception error) {
                    emitter.onError(error);
                }
            });
        });
    }

    public AmazonS3Client getS3Client(Context context) {
        if (sS3Client == null) {
            sS3Client = new AmazonS3Client(getCredProvider(context), Region.getRegion(Regions.AP_SOUTHEAST_1));
            try {
                String regionString = new AWSConfiguration(context)
                        .optJsonObject("S3TransferUtility")
                        .getString("Region");
                sS3Client.setRegion(Region.getRegion(regionString));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return sS3Client;
    }
}
