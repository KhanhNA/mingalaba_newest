package com.nextsolutions.newfeed.viewmodel;

import android.app.Application;
import android.util.Log;

import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.nextsolutions.AddLikeMutation;
import com.nextsolutions.DeletePostMutation;
import com.nextsolutions.DisLikeMutation;
import com.nextsolutions.OneItemQuery;
import com.nextsolutions.newfeed.adapter.GridImageAdapter;
import com.nextsolutions.newfeed.application.NewsFeedApplication;
import com.nextsolutions.newfeed.model.PostModel;
import com.nextsolutions.newfeed.util.GsonUTCDateAdapter;
import com.tsolution.base.BaseViewModel;
import com.tsolution.base.exceptionHandle.AppException;

import org.jetbrains.annotations.NotNull;

import java.util.Date;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PostDetailVM extends BaseViewModel {
    private ObservableBoolean isLoading = new ObservableBoolean();
    private ObservableField<PostModel> postDetail = new ObservableField();
    private GridImageAdapter imagePostAdapter;
    private CommentVM commentVM;
    //vị trí item vừa thao tác
    private int actionPosition;

    private int currentPage;
    private boolean isLoadFull;

    public PostDetailVM(@NonNull Application application) {
        super(application);
        postDetail.set(new PostModel());
        commentVM = new CommentVM(application);
    }


    public void getPostById(Integer itemId) {
        isLoading.set(true);
        NewsFeedApplication.getInstance().getApi().query(OneItemQuery.builder()
                .userId(NewsFeedApplication.getCurrentUserId())
                .itemId(itemId).build()
        ).enqueue(new ApolloCall.Callback<OneItemQuery.Data>() {
            @Override
            public void onResponse(@NotNull Response<OneItemQuery.Data> response) {
                isLoading.set(false);
                if (response.data() != null && response.data().oneItem() != null) {
                    GsonBuilder jsonBuilder = new GsonBuilder();
                    jsonBuilder.registerTypeAdapter(Date.class, new GsonUTCDateAdapter());
                    Gson gson = jsonBuilder.create();
                    String json = gson.toJson(response.data().oneItem());
                    PostModel post = gson.
                            fromJson(json, PostModel.class);
                    postDetail.set(post);
                    commentVM.getComments(post.getId(), 0);
                    try {
                        view.action("getPostSuccess", null, null, null);
                    } catch (AppException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {
            }
        });
    }

    public void deletePost(PostModel postModel) {
        NewsFeedApplication.getInstance().getApi().mutate(DeletePostMutation.builder().itemId(postModel.getId()).build()).enqueue(new ApolloCall.Callback<DeletePostMutation.Data>() {
            @Override
            public void onResponse(@NotNull Response<DeletePostMutation.Data> response) {
//                try {
//                    view.action(IntentConstants.DELETE_SUCCESS, null, null, null);
//                } catch (AppException e) {
//                    e.printStackTrace();
//                }
                Log.e("DELETE POST", "success");
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {
//                try {
//                    view.action(IntentConstants.UNFOLLOW_FAIL, null, null, null);
//                } catch (AppException ex) {
//                    ex.printStackTrace();
//                }
                Log.e("DELETE POST", "failure");

            }
        });
    }

    public void addLike(PostModel item) {
        NewsFeedApplication.getInstance().getApi().mutate(AddLikeMutation.builder().user_id(NewsFeedApplication.getCurrentUserId())
                .target_item_id(item.getId()).build()).enqueue(new ApolloCall.Callback<AddLikeMutation.Data>() {
            @Override
            public void onResponse(@NotNull Response<AddLikeMutation.Data> response) {
                try {
                    view.action("addLikeSuccess", null, null, null);
                } catch (AppException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {

            }
        });
    }

    public void disLike(PostModel item) {
        NewsFeedApplication.getInstance().getApi().mutate(DisLikeMutation.builder().user_id(NewsFeedApplication.getCurrentUserId())
                .target_item_id(item.getId()).build()).enqueue(new ApolloCall.Callback<DisLikeMutation.Data>() {
            @Override
            public void onResponse(@NotNull Response<DisLikeMutation.Data> response) {
                try {
                    view.action("addLikeSuccess", null, null, null);
                } catch (AppException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {
            }
        });
    }
}
