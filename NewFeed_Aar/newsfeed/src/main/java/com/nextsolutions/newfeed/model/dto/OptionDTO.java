package com.nextsolutions.newfeed.model.dto;

import com.tsolution.base.BaseModel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OptionDTO extends BaseModel {
    private String title;
    private String message;
    private Boolean isOnlyMe;


    public OptionDTO(String title, String message) {
        this.title = title;
        this.message = message;
    }
}
