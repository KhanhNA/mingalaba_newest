package nextsolutions.chat.listeners;

public interface ItemPositionChangedListener {
    void onItemPositionChangedListener(int position);
}
