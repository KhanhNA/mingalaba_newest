package nextsolutions.chat.preference

import android.content.Context
import androidx.preference.Preference
import android.util.AttributeSet
import android.view.View
import android.widget.TextView
import androidx.preference.EditTextPreference
import androidx.preference.PreferenceViewHolder
import nextsolutions.chat.R

open class DisplayNamePreference : Preference {


    internal var tvDisplayName:TextView? = null

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle)
    init {
        widgetLayoutResource = R.layout.display_name_preference
    }

    override fun onBindViewHolder(holder: PreferenceViewHolder) {
        super.onBindViewHolder(holder)
        tvDisplayName = holder.itemView.findViewById(R.id.tvDisplayName)
    }
    fun setDisplayName(displayName : String){
        tvDisplayName?.text = displayName
    }

}