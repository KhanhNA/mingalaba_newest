
package nextsolutions.chat.preference

import android.content.Context
import android.util.AttributeSet
import androidx.preference.Preference
import nextsolutions.chat.R

/**
 * Divider for Preference screen
 */
class VectorPreferenceDivider @JvmOverloads constructor(context: Context,
                                                        attrs: AttributeSet? = null,
                                                        defStyleAttr: Int = 0,
                                                        defStyleRes: Int = 0
) : Preference(context, attrs, defStyleAttr, defStyleRes) {



    init {
        layoutResource = R.layout.vector_preference_divider
    }
}