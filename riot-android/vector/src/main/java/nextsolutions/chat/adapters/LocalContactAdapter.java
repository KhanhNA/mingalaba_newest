package nextsolutions.chat.adapters;

import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import org.matrix.androidsdk.core.Log;
import org.matrix.androidsdk.core.MXPatterns;
import org.matrix.androidsdk.core.callback.SimpleApiCallback;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import nextsolutions.chat.R;
import nextsolutions.chat.contacts.ContactsManager;
import nextsolutions.chat.settings.VectorLocale;
import nextsolutions.chat.util.VectorUtils;

public class LocalContactAdapter extends AbsAdapter {

    private static final String LOG_TAG = LocalContactAdapter.class.getSimpleName();

    private static final int TYPE_HEADER_LOCAL_CONTACTS = 0;

    private static final int TYPE_CONTACT = 1;

    private final AdapterSection<ParticipantAdapterItem> mLocalContactsSection ;

    private final LocalContactAdapter.OnSelectItemListener mListener;

    private final String mNoContactAccessPlaceholder;
    private final String mNoResultPlaceholder;
    private final String mNoIdentityServerPlaceholder;

    private boolean isSelectedAll = false;
    private boolean hideButtonInvite = false;
    public int lstCheck = 0;

    /*
     * *********************************************************************************************
     * Constructor
     * *********************************************************************************************
     */

    public LocalContactAdapter(final Context context,
                         final LocalContactAdapter.OnSelectItemListener listener,
                         final RoomInvitationListener invitationListener,
                         final MoreRoomActionListener moreActionListener) {
        super(context, invitationListener, moreActionListener);
        mListener = listener;

        // ButterKnife.bind(this); cannot be applied here
        mNoContactAccessPlaceholder = context.getString(R.string.no_contact_access_placeholder);
        mNoResultPlaceholder = context.getString(R.string.no_result_placeholder);
        mNoIdentityServerPlaceholder = context.getString(R.string.people_no_identity_server);

        mLocalContactsSection = new AdapterSection<>(context,
                context.getString(R.string.invite_friend),
                -1,
                R.layout.adapter_item_local_contact_view,
                TYPE_HEADER_LOCAL_CONTACTS, TYPE_CONTACT,
                new ArrayList<ParticipantAdapterItem>(),
                ParticipantAdapterItem.alphaComparator);

        updateLocalContactsPlaceHolders();

        addSection(mLocalContactsSection);
    }

    public void selectAll(boolean isSelectedAll){
        this.isSelectedAll =  isSelectedAll;
        notifyDataSetChanged();
    }

    public void hideButtonApdaterInvite(boolean hideButtonInvite){
        this.hideButtonInvite = hideButtonInvite;
        notifyDataSetChanged();
    }

    private void updateLocalContactsPlaceHolders() {
        String noItemPlaceholder = mNoResultPlaceholder;
        if (!ContactsManager.getInstance().isContactBookAccessAllowed()) {
            noItemPlaceholder = mNoContactAccessPlaceholder;
        } else {
            if (mSession.getIdentityServerManager().getIdentityServerUrl() == null) {
                noItemPlaceholder = mNoIdentityServerPlaceholder;
            }
        }
        mLocalContactsSection.setEmptyViewPlaceholder(noItemPlaceholder);
    }

    /*
     * *********************************************************************************************
     * Abstract methods implementation
     * *********************************************************************************************
     */

    @Override
    protected RecyclerView.ViewHolder createSubViewHolder(ViewGroup viewGroup, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());

        View itemView;

        if (viewType == TYPE_HEADER_LOCAL_CONTACTS) {
            //TODO replace by a empty view ?
            itemView = inflater.inflate(R.layout.adapter_section_header_local, viewGroup, false);
            itemView.setBackgroundColor(Color.MAGENTA);
            return new HeaderViewHolder(itemView);
        } else {
            switch (viewType) {
                case TYPE_ROOM:
                    itemView = inflater.inflate(R.layout.adapter_item_room_view, viewGroup, false);
                    return new RoomViewHolder(itemView);
                case TYPE_CONTACT:
                    itemView = inflater.inflate(R.layout.adapter_item_local_contact_view, viewGroup, false);
                    return new LocalContactAdapter.ContactViewHolder(itemView);
            }
        }
        return null;
    }

    @Override
    protected void populateViewHolder(int viewType, RecyclerView.ViewHolder viewHolder, int position) {
        switch (viewType) {
            case TYPE_HEADER_LOCAL_CONTACTS:
                // Local header
                final HeaderViewHolder headerViewHolder = (HeaderViewHolder) viewHolder;
                for (Pair<Integer, AdapterSection> adapterSection : getSectionsArray()) {
                    if (adapterSection.first == position) {
                        headerViewHolder.populateViews(adapterSection.second);
                        break;
                    }
                }
                break;
            case TYPE_CONTACT:
                final LocalContactAdapter.ContactViewHolder contactViewHolder = (LocalContactAdapter.ContactViewHolder) viewHolder;
                final ParticipantAdapterItem item = (ParticipantAdapterItem) getItemForPosition(position);
                contactViewHolder.populateViews(item, position);
                break;
        }
    }

    @Override
    protected int applyFilter(String pattern) {
        int nbResults = 0;
//        nbResults += filterRoomSection(mDirectChatsSection, pattern);
        nbResults += filterLocalContacts(pattern);

        // if there is no pattern, use the local search
//        if (TextUtils.isEmpty(pattern)) {
//            nbResults += filterKnownContacts(pattern);
//        }
        return nbResults;
    }



    public void setLocalContacts(final List<ParticipantAdapterItem> localContacts) {
        // updates the placeholder according to the local contacts permissions
        updateLocalContactsPlaceHolders();
        mLocalContactsSection.setItems(localContacts, mCurrentFilterPattern);
        if (!TextUtils.isEmpty(mCurrentFilterPattern)) {
            filterLocalContacts(String.valueOf(mCurrentFilterPattern));
        }
        updateSections();
    }

//    public void setFilteredKnownContacts(List<ParticipantAdapterItem> filteredKnownContacts, String pattern) {
//        Collections.sort(filteredKnownContacts, ParticipantAdapterItem.getComparator(mSession));
//        mKnownContactsSection.setFilteredItems(filteredKnownContacts, pattern);
//        updateSections();
//    }

    /**
     * Filter the local contacts with the given pattern
     *
     * @param pattern
     * @return nb of items matching the filter
     */
    private int filterLocalContacts(final String pattern) {
        if (!TextUtils.isEmpty(pattern)) {
            List<ParticipantAdapterItem> filteredLocalContacts = new ArrayList<>();
            final String formattedPattern = pattern.toLowerCase(VectorLocale.INSTANCE.getApplicationLocale()).trim();

            List<ParticipantAdapterItem> sectionItems = new ArrayList<>(mLocalContactsSection.getItems());
            for (final ParticipantAdapterItem item : sectionItems) {
                if (item.startsWith(formattedPattern)) {
                    filteredLocalContacts.add(item);
                }
            }
            mLocalContactsSection.setFilteredItems(filteredLocalContacts, pattern);
        } else {
            mLocalContactsSection.resetFilter();
        }

        return mLocalContactsSection.getFilteredItems().size();
    }



    /*
     * *********************************************************************************************
     * View holder
     * *********************************************************************************************
     */

    class ContactViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.contact_view)
        RelativeLayout contactView;

        @BindView(R.id.adapter_item_local_contact_avatar)
        ImageView vContactAvatar;

        @BindView(R.id.local_contact_badge)
        ImageView vContactBadge;

        @BindView(R.id.local_contact_name)
        TextView vContactName;

        @BindView(R.id.local_contact_desc)
        TextView vContactDesc;

        @BindView(R.id.btn_invite_friend)
        Button btnInvite;

        @BindView(R.id.cbContact)
        CheckBox cbContact;



        private ContactViewHolder(final View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }



        private void populateViews(ParticipantAdapterItem participant, final int position) {


            if (!isSelectedAll) {
                cbContact.setChecked(false);


            } else{

                cbContact.setChecked(true);
            }

            if (hideButtonInvite){
                btnInvite.setVisibility(View.GONE);
                cbContact.setVisibility(View.VISIBLE);
            }else {
                btnInvite.setVisibility(View.VISIBLE);
                cbContact.setVisibility(View.GONE);
            }

            if (null == participant) {
                Log.e(LOG_TAG, "## populateViews() : null participant");
                return;
            }

            if (position >= getItemCount()) {
                Log.e(LOG_TAG, "## populateViews() : position out of bound " + position + " / " + getItemCount());
                return;
            }

            participant.displayAvatar(mSession, vContactAvatar);
            vContactName.setText(participant.getUniqueDisplayName(null));

            /*
             * Get the description to be displayed below the name
             * For local contact, it is the medium (email, phone number)
             * For other contacts, it is the presence
             */
            if (participant.mContact != null) {
                boolean isMatrixUserId = MXPatterns.isUserId(participant.mUserId);
                vContactBadge.setVisibility(isMatrixUserId ? View.VISIBLE : View.GONE);

                if (participant.mContact.getEmails().size() > 0) {
                    vContactDesc.setText(participant.mContact.getEmails().get(0));
                } else {
                    vContactDesc.setText(participant.mContact.getPhonenumbers().get(0).mRawPhoneNumber);
                }
            } else {
                loadContactPresence(vContactDesc, participant, position);
                vContactBadge.setVisibility(View.GONE);
            }

            participant.checkedInviteContact = cbContact.isChecked();
            cbContact.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    participant.checkedInviteContact = isChecked;
                    // count contact checked to disable button invite
                    if (isChecked){
                        lstCheck = lstCheck +1;
                    }else if (lstCheck > 0){
                        lstCheck = lstCheck -1;
                    }
                    mListener.onSelectItem(participant,cbContact,-1);

                }
            });

            btnInvite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   mListener.onSelectItem(participant,v,-1);
                }
            });
            contactView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    mListener.onSelectItem(participant,v,-1);
                    return false;
                }
            });
        }

        /**
         * Get the presence for the given contact
         *
         * @param textView
         * @param item
         * @param position
         */
        private void loadContactPresence(final TextView textView, final ParticipantAdapterItem item,
                                         final int position) {
            final String presence = VectorUtils.getUserOnlineStatus(mContext, mSession, item.mUserId, new SimpleApiCallback<Void>() {
                @Override
                public void onSuccess(Void info) {
                    if (textView != null) {
                        textView.setText(VectorUtils.getUserOnlineStatus(mContext, mSession, item.mUserId, null));
                        notifyItemChanged(position);
                    }
                }
            });
            textView.setText(presence);
        }
    }

    /*
     * *********************************************************************************************
     * Inner classes
     * *********************************************************************************************
     */

    public interface OnSelectItemListener {
        void onSelectItem(ParticipantAdapterItem item, View view,int position);
    }
}
