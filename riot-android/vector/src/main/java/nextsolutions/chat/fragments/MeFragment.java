package nextsolutions.chat.fragments;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import org.matrix.androidsdk.data.Room;

import java.util.List;

import butterknife.BindView;
import nextsolutions.chat.R;
import nextsolutions.chat.ui.themes.ThemeUtils;
import nextsolutions.chat.util.VectorUtils;

public class MeFragment extends AbsHomeFragment {

    @BindView(R.id.me_avatar)
    ImageView me_avatar;

    @BindView(R.id.txtName)
    TextView txtDisplayName;

    public static MeFragment newInstance(){
        return new MeFragment();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mPrimaryColor = ThemeUtils.INSTANCE.getColor(getActivity(), R.attr.vctr_tab_home);
        mSecondaryColor = ThemeUtils.INSTANCE.getColor(getActivity(), R.attr.vctr_tab_home_secondary);

        initView();
    }

    private void initView() {
        VectorUtils.loadUserAvatar(getContext(), mSession, me_avatar, mSession.getMyUser());
        txtDisplayName.setText(mSession.getMyUser().displayname);
    }

    @Override
    protected List<Room> getRooms() {
        return null;
    }

    @Override
    protected void onFilter(String pattern, OnFilterListener listener) {

    }

    @Override
    protected void onResetFilter() {

    }

    @Override
    public int getLayoutResId() {
        return R.layout.fragment_me;
    }
}
