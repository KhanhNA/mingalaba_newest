package nextsolutions.chat.fragments;

import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.tsolution.base.BaseFragment;
import com.tsolution.base.BaseViewModel;

import org.matrix.androidsdk.data.Room;

import java.util.List;

import butterknife.BindView;
import nextsolutions.chat.R;
import nextsolutions.chat.contacts.ContactPaperAdapter;
import nextsolutions.chat.contacts.ContactVM;
import nextsolutions.chat.widgets.SlidingTabLayout;

public class ContactFragment extends AbsHomeFragment {
    @BindView(R.id.slidingTab)
    SlidingTabLayout slidingTabs;

    @BindView(R.id.vpContent)
    ViewPager vpTab;

    ContactPaperAdapter myPagerAdapter;
    private boolean isLoad = true;

    public static ContactFragment newInstance() {
        return new ContactFragment();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        innitView();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    protected List<Room> getRooms() {
        return null;
    }

    private static String makeFragmentName(int viewId, long id) {
        return "android:switcher:" + viewId + ":" + id;
    }

    @Override
    protected void onFilter(String pattern, OnFilterListener listener) {

        // get tag fragment to find search view
        Fragment myFragment = (Fragment) myPagerAdapter.instantiateItem(vpTab,vpTab.getCurrentItem());

        if (myFragment instanceof AbsHomeFragment) {
            ((AbsHomeFragment) myFragment).applyFilter(pattern.trim());
        }
    }

    @Override
    protected void onResetFilter() {
        // get tag fragment to find search view
        Fragment fragment = (Fragment) myPagerAdapter.instantiateItem(vpTab,vpTab.getCurrentItem());

        if (fragment instanceof AbsHomeFragment) {
            ((AbsHomeFragment) fragment).applyFilter("");
        }
    }

    private void innitView() {
//        slidingTabs = v.findViewById(R.id.slidingTab);
//        vpTab = v.findViewById(R.id.vpContent);
        slidingTabs.setCustomTabView(R.layout.tab_indicator, android.R.id.text1);
        slidingTabs.setSelectedIndicatorColors(getResources().getColor(R.color.primaryColor));
        slidingTabs.setDistributeEvenly(true);
//        if(isLoad)
//        {
            isLoad =false;
            myPagerAdapter = new ContactPaperAdapter(getChildFragmentManager(), getResources().getStringArray(R.array.title_tab_contact));
            vpTab.setAdapter(myPagerAdapter);
            vpTab.setOffscreenPageLimit(1);
            vpTab.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                    Log.d("onPageSelected",""+position);
                }

                @Override
                public void onPageSelected(int position) {
                    Log.d("onPageSelected",""+position);
                }

                @Override
                public void onPageScrollStateChanged(int state) {
                    Log.d("onPageSelected",""+state);
                }
            });
            slidingTabs.setViewPager(vpTab);
            final int pageMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, getResources()
                    .getDisplayMetrics());
            vpTab.setPageMargin(pageMargin);
            vpTab.setCurrentItem(0);
//        }
    }

    @Override
    public int getLayoutResId() {
        return R.layout.fragment_contact;
    }
}
