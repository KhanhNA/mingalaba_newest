package nextsolutions.chat.fragments;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.Filter;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.matrix.androidsdk.core.Log;
import org.matrix.androidsdk.data.Room;
import org.matrix.androidsdk.features.terms.TermsManager;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import nextsolutions.chat.R;
import nextsolutions.chat.activity.ReviewTermsActivity;
import nextsolutions.chat.activity.util.RequestCodesKt;
import nextsolutions.chat.adapters.LocalContactAdapter;
import nextsolutions.chat.adapters.ParticipantAdapterItem;
import nextsolutions.chat.contacts.Contact;
import nextsolutions.chat.contacts.ContactsManager;
import nextsolutions.chat.contacts.PIDsRetriever;
import nextsolutions.chat.dialogs.DialogWithEditText;
import nextsolutions.chat.ui.themes.ThemeUtils;
import nextsolutions.chat.util.HomeRoomsViewModel;
import nextsolutions.chat.util.PermissionsToolsKt;
import nextsolutions.chat.view.EmptyViewItemDecoration;
import nextsolutions.chat.view.SimpleDividerItemDecoration;

public class LocalContactFragment extends AbsHomeFragment implements ContactsManager.ContactsManagerListener, AbsHomeFragment.OnRoomChangedListener {
    private static final String LOG_TAG = LocalContactFragment.class.getSimpleName();

    @BindView(R.id.recyclerview)
    RecyclerView mRecycler;

    @BindView(R.id.btnInvite)
    Button btnInvite;

    @BindView(R.id.btnCheckAll)
    Button btnCheckAll;

    @BindView(R.id.ll_invite)
    LinearLayout llInvite;

    private LocalContactAdapter mAdapter;

    private final List<ParticipantAdapterItem> mLocalContacts = new ArrayList<>();

    // way to detect that the contacts list has been updated
    private int mContactsSnapshotSession = -1;

    private boolean isCheckAllContact = false;

    private boolean checkShowCheckAll = false;

    /*
     * *********************************************************************************************
     * Static methods
     * *********************************************************************************************
     */

    public static LocalContactFragment newInstance() {
        return new LocalContactFragment();
    }

    /*
     * *********************************************************************************************
     * Fragment lifecycle
     * *********************************************************************************************
     */

    @Override
    public int getLayoutResId() {
        return R.layout.local_contact_fragment;
    }

    @Override
    public void onActivityCreated(final Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mPrimaryColor = ThemeUtils.INSTANCE.getColor(getActivity(), R.attr.vctr_tab_home);
        mSecondaryColor = ThemeUtils.INSTANCE.getColor(getActivity(), R.attr.vctr_tab_home_secondary);

        mFabColor = ContextCompat.getColor(getActivity(), R.color.tab_rooms);
        mFabPressedColor = ContextCompat.getColor(getActivity(), R.color.tab_rooms_secondary);

        initViews();

        mOnRoomChangedListener = this;

        mAdapter.onFilterDone(mCurrentFilter);

        if (!ContactsManager.getInstance().isContactBookAccessRequested()) {
            PermissionsToolsKt.checkPermissions(PermissionsToolsKt.PERMISSIONS_FOR_MEMBERS_SEARCH, this, PermissionsToolsKt.PERMISSION_REQUEST_CODE);
        }

    }

    @OnClick(R.id.btnCheckAll)
    void checkAllContact(){
        isCheckAllContact = !isCheckAllContact;
        // intialization position = 2 so when check all declares lst check = 2
        mAdapter.lstCheck = 2;
        mAdapter.selectAll(isCheckAllContact);
    }

    @OnClick(R.id.btnInvite)
    void sendSmsToContact(){
        dialogShare(null);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
    }

    @Override
    public void onResume() {
        super.onResume();
        ContactsManager.getInstance().addListener(this);
        // Local address book
        initContactsData();
        initContactsViews();

        mAdapter.setInvitation(mActivity.getRoomInvitations());

        mRecycler.addOnScrollListener(mScrollListener);
    }

    @Override
    public void onPause() {
        super.onPause();

        if (mSession.isAlive()) {
        }
        ContactsManager.getInstance().removeListener(this);

        mRecycler.removeOnScrollListener(mScrollListener);

        // cancel any search
        mSession.cancelUsersSearch();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PermissionsToolsKt.PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                ContactsManager.getInstance().refreshLocalContactsSnapshot();
            } else {
                initContactsData();
            }

            // refresh the contact views
            // the placeholders might need to be updated
            initContactsViews();
        }
    }

    /*
     * *********************************************************************************************
     * Abstract methods implementation
     * *********************************************************************************************
     */

    @Override
    protected List<Room> getRooms() {
        return null;
    }

    @Override
    protected void onFilter(final String pattern, final OnFilterListener listener) {
        if (mAdapter!= null) {
            mAdapter.getFilter().filter(pattern, new Filter.FilterListener() {
                @Override
                public void onFilterComplete(int count) {
                    boolean newSearch = TextUtils.isEmpty(mCurrentFilter) && !TextUtils.isEmpty(pattern);

                    Log.i(LOG_TAG, "onFilterComplete " + count);
                    if (listener != null) {
                        listener.onFilterDone(count);
                    }
                }
            });
        }
    }

    @Override
    protected void onResetFilter() {
        mAdapter.getFilter().filter("", new Filter.FilterListener() {
            @Override
            public void onFilterComplete(int count) {
                Log.i(LOG_TAG, "onResetFilter " + count);
            }
        });
    }

    /*
     * *********************************************************************************************
     * UI management
     * *********************************************************************************************
     */

    /**
     * Prepare views
     */
    private void initViews() {
        int margin = (int) getResources().getDimension(R.dimen.item_decoration_left_margin);
        mRecycler.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false));
        mRecycler.addItemDecoration(new SimpleDividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL, margin));
        mRecycler.addItemDecoration(new EmptyViewItemDecoration(getActivity(), DividerItemDecoration.VERTICAL, 40, 16, 14));
        mAdapter = new LocalContactAdapter(getActivity(), new LocalContactAdapter.OnSelectItemListener() {

            @Override
            public void onSelectItem(ParticipantAdapterItem contact, View view, int position) {
                if (view.getId() == R.id.btn_invite_friend){
                    dialogShare(contact);
                } else if (view.getId() == R.id.cbContact){
                    enableButtonInvite();
                } else if(view.getId() == R.id.contact_view){
                    checkShowCheckAll = !checkShowCheckAll;
                    if (checkShowCheckAll) {
                        showButtonCheckAll();
                    }else {
                        llInvite.setVisibility(View.GONE);
                        mAdapter.hideButtonApdaterInvite(false);
                    }
                }
            }

        }, this, this);
        mRecycler.setAdapter(mAdapter);
    }

    private void showButtonCheckAll() {
        llInvite.setVisibility(View.VISIBLE);
        enableButtonInvite();
        mAdapter.hideButtonApdaterInvite(true);
    }

    private void enableButtonInvite() {
        if (mAdapter.lstCheck == 0){
            btnInvite.setEnabled(false);
        }else {
            btnInvite.setEnabled(true);
        }
        String textBtnInvite = getResources().getString(R.string.invite) + " "+mAdapter.lstCheck;
        btnInvite.setText(textBtnInvite);
    }

    private void dialogShare(ParticipantAdapterItem contact) {
        StringBuilder contactPhones = new StringBuilder();
        StringBuilder contactNames = new StringBuilder();
        //lấy danh sách contact được check
        if(contact == null){
            for (ParticipantAdapterItem dto : mLocalContacts) {
                if (dto.checkedInviteContact) {
                    contactPhones.append(dto.mContact.getPhonenumbers().get(0).mRawPhoneNumber);
                    contactNames.append((dto.mDisplayName)).append(", ");
                    if (Build.MANUFACTURER.equalsIgnoreCase("Samsung")) {
                        contactPhones.append(", ");
                    } else {
                        contactPhones.append("; ");
                    }
                }
            }
        }else {
            contactNames.append(contact.mDisplayName);
            contactPhones.append(contact.mContact.getPhonenumbers().get(0).mRawPhoneNumber);
        }

        DialogWithEditText shareCodeDialog = new DialogWithEditText(getResources().getString(R.string.send_invitations_to_friends),
                getString(R.string.message_sms_invite) + " " + contactNames.toString(),
                v -> {
                    String message_share = "Mingalaba Chat\n" + String.format(getString(R.string.msg_invite_people), mSession.getMyUser().displayname, "http://demo.nextsolutions.com.vn/app.apk", v.getText().toString());
                    Intent intent = new Intent(Intent.ACTION_SENDTO);
                    intent.setData(Uri.parse("smsto:" + Uri.encode(contactPhones.toString())));
                    intent.putExtra("sms_body", message_share);
                    startActivity(intent);
                }
        );

        shareCodeDialog.show(getChildFragmentManager(), shareCodeDialog.getTag());
//
    }



    /**
     * Fill the local address book and known contacts adapters with data
     */
    private void initContactsData() {
//        ContactsManager.getInstance().retrievePids();
        ContactsManager.getInstance().getPhoneNumberThreePid();


        if (mContactsSnapshotSession == -1
                || mContactsSnapshotSession != ContactsManager.getInstance().getLocalContactsSnapshotSession()
                || !ContactsManager.getInstance().didPopulateLocalContacts()) {
            // First time on the screen or contact data outdated
            mLocalContacts.clear();
            List<ParticipantAdapterItem> participants = new ArrayList<>(getContacts());
            // Build lists
            for (ParticipantAdapterItem item : participants) {
                if (item.mContact != null) {
                    mLocalContacts.add(item);
                }
            }
        }
    }



    /**
     * Retrieve the contacts
     *
     * @return
     */
    private List<ParticipantAdapterItem> getContacts() {
        List<ParticipantAdapterItem> participants = new ArrayList<>();

        Collection<Contact> contacts = ContactsManager.getInstance().getLocalContactsSnapshot();
        mContactsSnapshotSession = ContactsManager.getInstance().getLocalContactsSnapshotSession();

        if (null != contacts) {
            for (Contact contact : contacts) {

                for (Contact.PhoneNumber pn : contact.getPhonenumbers()) {
//                    Contact.MXID mxid = PIDsRetriever.getInstance().getMXID(pn.mMsisdnPhoneNumber);
                    // check id chat has or not
                    Contact.ContactIdentify mxid = PIDsRetriever.getInstance().getContactIdentify(pn.mMsisdnPhoneNumber);

                    if (null == mxid) {
                        Contact dummyContact = new Contact(pn.mMsisdnPhoneNumber);
                        dummyContact.setDisplayName(contact.getDisplayName());
                        dummyContact.addPhoneNumber(pn.mRawPhoneNumber, pn.mE164PhoneNumber);
                        dummyContact.setThumbnailUri(contact.getThumbnailUri());
                        ParticipantAdapterItem participant = new ParticipantAdapterItem(dummyContact);
//                        participant.mUserId = mxid.mUserId;
                        participants.add(participant);
                    }
                }
            }
        }

        return participants;
    }


    /**
     * Init contacts views with data and update their display
     */
    private void initContactsViews() {
        mAdapter.setLocalContacts(mLocalContacts);
    }

    /*
     * *********************************************************************************************
     * Listeners
     * *********************************************************************************************
     */

    @Override
    public void onRoomResultUpdated(final HomeRoomsViewModel.Result result) {
    }

    @Override
    public void onRefresh() {
        initContactsData();
        initContactsViews();
    }

    @Override
    public void onPIDsUpdate() {
        final List<ParticipantAdapterItem> newContactList = getContacts();
        if (!mLocalContacts.containsAll(newContactList)) {
            mLocalContacts.clear();
            mLocalContacts.addAll(newContactList);
            initContactsViews();
        }
    }

    @Override
    public void onContactPresenceUpdate(Contact contact, String matrixId) {
        //TODO
    }

    @Override
    public void onIdentityServerTermsNotSigned(String token) {
        if (isAdded()) {
            startActivityForResult(ReviewTermsActivity.Companion.intent(getActivity(),
                    TermsManager.ServiceType.IdentityService, mSession.getIdentityServerManager().getIdentityServerUrl() /* Cannot be null */, token),
                    RequestCodesKt.TERMS_REQUEST_CODE);
        }
    }

    @Override
    public void onNoIdentityServerDefined() {

    }

    @Override
    public void onToggleDirectChat(String roomId, boolean isDirectChat) {
    }

    @Override
    public void onRoomLeft(String roomId) {
    }

    @Override
    public void onRoomForgot(String roomId) {
    }
}
