package nextsolutions.chat.dialogs

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.fragment.app.DialogFragment
import kotlinx.android.synthetic.main.dialog_custom.view.*
import nextsolutions.chat.R

class DialogCustom(private var title: String,
                   private var mes:String,
                   private  var onClickListener: View.OnClickListener) : DialogFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.dialog_custom, container, false)

        if (dialog != null && dialog!!.window != null) {
            dialog?.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog?.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        }
        view.txtTitle.text = title
        view.txtMsg.text = mes
        view.btnConfirm.setOnClickListener(onClickListener)
        view.btnCancel.setOnClickListener { v -> this.dismiss() }
//        view.btnDismiss.setOnClickListener { v -> this.dismiss() }
        return view
    }
}