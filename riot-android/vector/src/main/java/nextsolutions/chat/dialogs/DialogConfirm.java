package nextsolutions.chat.dialogs;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;


import com.nextsolutions.newfeed.util.StringUtils;

import nextsolutions.chat.R;


public class DialogConfirm extends DialogFragment {
    private String title;
    private String actionTitle;
    private String msg;
    private View.OnClickListener onClickListener;
    private boolean isCancelable;
    // show or gone btn dismiss
    private boolean isShowDisable = false;
    // use click or not btn Cancel
    private boolean isUseOnclickCancel = false;



    public DialogConfirm(@NonNull String title, String msg,boolean isShowDiable ,View.OnClickListener onClickListener) {
        this.msg = msg;
        this.title = title;
        this.onClickListener = onClickListener;
        this.isCancelable = false;
        this.isShowDisable = isShowDisable;
        isUseOnclickCancel = true;
    }

    public DialogConfirm(@NonNull String title, String msg, String actionTitle, View.OnClickListener onClickListener) {
        this.msg = msg;
        this.title = title;
        this.onClickListener = onClickListener;
        this.actionTitle = actionTitle;
        this.isCancelable = true;
    }

    public DialogConfirm(@NonNull String title, String msg,boolean isShowDiable ,boolean isUseOnclickCancel,View.OnClickListener onClickListener) {
        this.msg = msg;
        this.title = title;
        this.onClickListener = onClickListener;
        this.isCancelable = false;
        this.isShowDisable = isShowDiable;
        this.isUseOnclickCancel = isUseOnclickCancel;
    }

    public DialogConfirm(){

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.dialog_confirm, container, false);

        if (getDialog() != null && getDialog().getWindow() != null) {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            getDialog().getWindow().requestFeature(Window .FEATURE_NO_TITLE);
        }
        Button btnCancel = view.findViewById(R.id.btnCancel);
        ImageView btnDismiss = view.findViewById(R.id.btnDismiss);
        Button btnConfirm = view.findViewById(R.id.btnConfirm);
        TextView txtTitle = view.findViewById(R.id.txtTitle);
        TextView txtMsg = view.findViewById(R.id.txtMsg);


        setCancelable(isCancelable);
//        btnCancel.setVisibility(isCancelable ? View.VISIBLE : View.INVISIBLE);
        btnDismiss.setOnClickListener(v -> this.dismiss());
        btnDismiss.setVisibility(isShowDisable? View.VISIBLE : View.GONE);

//
        if(actionTitle != null){
            btnConfirm.setText(actionTitle);
        }
        if (StringUtils.isNullOrEmpty(msg)) {
            txtMsg.setVisibility(View.GONE);
        } else {
            txtMsg.setText(msg);
        }

        if(StringUtils.isNullOrEmpty(title)){
            txtTitle.setVisibility(View.GONE);
        }else {
            txtTitle.setText(title);
        }

//
        btnCancel.setOnClickListener(isUseOnclickCancel? onClickListener:(v->this.dismiss()));
//        btnCancel.setOnClickListener(v ->this.dismiss());
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickListener.onClick(v);
                dismiss();
            }
        });

        return view;
    }


}