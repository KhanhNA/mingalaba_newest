package nextsolutions.chat.dialogs;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.preference.Preference;

import com.nextsolutions.newfeed.util.StringUtils;

import nextsolutions.chat.R;

public class DialogSeclectGender extends DialogFragment {

    private OnSelectGender onSelectGender;
    private int typeGender;
    private RadioButton rdMale, rdFemale, rdOther;


    public DialogSeclectGender(int typeGender, OnSelectGender onSelectGender) {
        this.typeGender = typeGender;
        this.onSelectGender = onSelectGender;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.dialog_select_gender, container, false);

        if (getDialog() != null && getDialog().getWindow() != null) {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }

        rdMale = view.findViewById(R.id.radioMale);
        rdFemale = view.findViewById(R.id.radioFemale);
        rdOther = view.findViewById(R.id.radioOther);

        setTypeGenderSelect();

        RadioGroup radioButton = view.findViewById(R.id.radioGroup);
        radioButton.setOnCheckedChangeListener((group,checkedId) -> {
            switch (checkedId){
                case R.id.radioMale:
                    onSelectGender.onSelectGender(0);
                    this.dismiss();
                    break;
                case R.id.radioFemale:
                    onSelectGender.onSelectGender(1);
                    this.dismiss();
                    break;
                case R.id.radioOther:
                    onSelectGender.onSelectGender(2);
                    this.dismiss();
                    break;
            }
        });


        return view;
    }

    private void setTypeGenderSelect() {
        if (typeGender == 0){
            rdMale.setChecked(true);
        }else if (typeGender == 1){
            rdFemale.setChecked(true);
        }else if (typeGender == 2){
            rdOther.setChecked(true);
        }
    }

    public interface OnSelectGender {
        void onSelectGender(int typeGender);
    }
}

