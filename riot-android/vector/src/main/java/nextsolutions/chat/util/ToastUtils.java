package nextsolutions.chat.util;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import nextsolutions.chat.R;
import nextsolutions.chat.VectorApp;


/**
 */
public class ToastUtils {


    private static String oldMsg;

    private static Toast toast = null;

    private static long oneTime = 0;

    private static long twoTime = 0;

    public static void showToast(String s) {
        try {
            if (null == s) {
                return;
            }
            if (toast == null) {
                toast = Toast.makeText(VectorApp.getInstance(), s, Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
                oneTime = System.currentTimeMillis();
            } else {
                twoTime = System.currentTimeMillis();
                if (s.equals(oldMsg)) {
                    if (twoTime - oneTime > Toast.LENGTH_SHORT) {
                        toast.show();
                    }
                } else {
                    oldMsg = s;
                    toast.setText(s);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
            }
            oneTime = twoTime;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void showToast(Integer res) {
        if (null == res) {
            return;
        }
        String s = VectorApp.getInstance().getResources().getString(res);
        if (toast == null) {
            toast = Toast.makeText(VectorApp.getInstance(), s, Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
            oneTime = System.currentTimeMillis();
        } else {
            twoTime = System.currentTimeMillis();
            if (s.equals(oldMsg)) {
                if (twoTime - oneTime > Toast.LENGTH_SHORT) {
                    toast.show();
                }
            } else {
                oldMsg = s;
                toast.setText(s);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
        }
        oneTime = twoTime;
    }

    public static void showToast(Activity activity, Integer res, Integer container) {
        if (null == res || activity == null) {
            return;
        }
        LayoutInflater inflater = activity.getLayoutInflater();
        View layout = inflater.inflate(res, activity.findViewById(container));
        Toast mToast = new Toast(VectorApp.getInstance());
        mToast.setDuration(Toast.LENGTH_SHORT);
        mToast.setView(layout);
        mToast.setGravity(Gravity.CENTER, 0, 0);
        mToast.show();
    }

    public static void showToast(Context context, String s, int duration) {
        try {
            if (TextUtils.isEmpty(s)) {
                return;
            }
            if (toast == null) {
                toast = Toast.makeText(context, s, duration);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
                oneTime = System.currentTimeMillis();
            } else {
                twoTime = System.currentTimeMillis();

                if (s.equals(oldMsg)) {
                    if (twoTime - oneTime > duration) {
                        toast.show();
                    }
                } else {
                    oldMsg = s;
                    toast.setText(s);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
            }
            oneTime = twoTime;
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void showToast(Activity activity, @NonNull String res, Drawable icon) {
        if (activity == null) {
            return;
        }
        LayoutInflater inflater = activity.getLayoutInflater();
        View layout = inflater.inflate(R.layout.custom_toast_view,activity.findViewById(R.id.rootView));
        TextView textView = layout.findViewById(R.id.text);
        textView.setText(res);
        ImageView imageView = layout.findViewById(R.id.icon);
        imageView.setImageDrawable(icon);
        Toast mToast  = new Toast(VectorApp.getInstance());
        mToast.setDuration(Toast.LENGTH_SHORT);
        mToast.setView(layout);
        mToast.setGravity(Gravity.CENTER, 0, 0);
        mToast.show();
    }
}
