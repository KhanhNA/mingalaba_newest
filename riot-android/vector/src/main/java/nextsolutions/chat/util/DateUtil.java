package nextsolutions.chat.util;

import android.annotation.SuppressLint;
import android.text.TextUtils;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {
    public static final String Format_YYYY_MM_DD = "yyyy-mm-dd";

    @SuppressLint("SimpleDateFormat")
    public static Date convertStringToDate(String date) {
        Date toDate = null;
        SimpleDateFormat format = new SimpleDateFormat(Format_YYYY_MM_DD);
        try {
            toDate = format.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return toDate;
    }

    @SuppressLint("SimpleDateFormat")
    public static int checkDates(String startDate, String endDate) {
        SimpleDateFormat dfDate = new SimpleDateFormat(Format_YYYY_MM_DD);
        int b = -2;

        try {
            if (!TextUtils.isEmpty(startDate) && !TextUtils.isEmpty(endDate)) {
                if (dfDate.parse(startDate).before(dfDate.parse(endDate))) {
                    b = 1;  // If start date is before end date.
                } else if (dfDate.parse(startDate).equals(dfDate.parse(endDate))) {
                    b = -1;  // If two dates are equal.
                } else {
                    b = 0; // If start date is after the end date.
                }
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return b;
    }


}
