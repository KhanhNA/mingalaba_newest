package nextsolutions.chat.contacts;

import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import nextsolutions.chat.fragments.FriendContactFragment;
import nextsolutions.chat.fragments.LocalContactFragment;


public class ContactPaperAdapter extends FragmentPagerAdapter {
    private String[] TITLES;

    public ContactPaperAdapter(FragmentManager fm, String[] title) {
        super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        this.TITLES = title;
    }



    @Override
    public CharSequence getPageTitle(int position) {
        return TITLES[position];
    }

    @Override
    public int getCount() {
        return TITLES.length;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0: //friend contact
                return new FriendContactFragment();
            case 1: //contact
                return new LocalContactFragment();
            default:
                return null;

        }

    }

}